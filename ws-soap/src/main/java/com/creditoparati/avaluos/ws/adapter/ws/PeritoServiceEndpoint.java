/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.adapter.ws;

import com.creditoparati.avaluos.ws.api.PeritoService;
import com.creditoparati.avaluos.ws.domain.Perito;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * Web Service de Perito.
 * @author Edgar Deloera
 */
@Endpoint
public class PeritoServiceEndpoint {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(PeritoServiceEndpoint.class);

    private static final String NAMESPACE_URI = "http://sat.gob.mx/perito/schemas";

    private XPathExpression<Element> idPeritoExpression;

    private XPathExpression<Element> subtotalExpression;

    private XPathExpression<Element> ivaExpression;

    private XPathExpression<Element> totalExpression;

    private XPathExpression<Element> rfcExpression;

    private PeritoService peritoApiService;

    private static Namespace NS = Namespace.getNamespace("perito", NAMESPACE_URI);

    @Autowired
    public PeritoServiceEndpoint(PeritoService PeritoApiService) throws JDOMException {
        this.peritoApiService = PeritoApiService;
        XPathFactory xPathFactory = XPathFactory.instance();
        idPeritoExpression = xPathFactory.compile("//perito:idPerito", Filters.element(), null, NS);
        rfcExpression = xPathFactory.compile("//perito:rfc", Filters.element(), null, NS);
        subtotalExpression = xPathFactory.compile("//perito:subtotal", Filters.element(), null, NS);
        ivaExpression = xPathFactory.compile("//perito:iva", Filters.element(), null, NS);
        totalExpression = xPathFactory.compile("//perito:total", Filters.element(), null, NS);
    }

    /**
     * Atiende las operaciones que necesitan el detalle de un Perito.
     * @param PeritoDetailRequest La peticion de detalle de Perito.
     * @return El mensaje XML con el detalle del Perito.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "peritoDetailRequest")
    @ResponsePayload
    public Element handlePeritoDetailRequest(@RequestPayload Element PeritoDetailRequest) 
           throws Exception {
        LOG.info("### PeritoServiceEndpoint.handlePeritoDetailRequest...");
        String idPerito = idPeritoExpression.evaluateFirst(PeritoDetailRequest).getText();
        LOG.debug("### >> idPerito=[" + idPerito + "]");

        Perito perito = null;
        perito = this.peritoApiService.findPerito(new Long(idPerito));

        return createDetailReturnXML(perito);
    }

    /**
     * Genera el XML del detalle del Perito.
     * @param perito El Perito para obtener el detalle.
     * @return Retorna el elemento XML con el detalle del Perito.
     */
    private Element createDetailReturnXML(Perito perito) {
        Element peritoResponse = new Element("peritoDetailResponse", NS);
        Element peritoElement = new Element("perito", NS);
        peritoResponse.addContent(peritoElement);
        peritoElement.addContent(createElement("id", perito.getIdPerito().toString()));
        peritoElement.addContent(createElement("rfc", perito.getRfc()));
        peritoElement.addContent(createElement("subtotal", perito.getSubtotal().toString()));
        peritoElement.addContent(createElement("iva", perito.getIva().toString()));
        peritoElement.addContent(createElement("total", perito.getTotal().toString()));

        return peritoResponse;
    }

    /**
     * Genera un elemento a adicionar en otro elemento contenedor. 
     * @param elementName El nombre del elemento a generar.
     * @param elementValue El valor del elemento a incluir.
     * @return El elemento para ser almacenado en otro. 
     */
    private Element createElement(String elementName, String elementValue) {

        Element element = new Element(elementName, NS);
        element.setText(elementValue);
        return element;
    }

    /**
     * Atiende las operaciones que necesitan insertar un Perito.
     * @param PeritoInsertRequest La peticion de insertar el detalle con los datos del
     *                                   Perito.
     * @return El mensaje XML con el resultado de la insercion del Perito.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "peritoInsertRequest")
    @ResponsePayload
    public Element handlePeritoInsertRequest(@RequestPayload Element peritoInsertRequest) 
           throws Exception {
        LOG.info("### PeritoServiceEndpoint.handlePeritoInsertRequest...");
        String rfc = rfcExpression.evaluateFirst(peritoInsertRequest).getText();
        LOG.debug("### >> rfc=[" + rfc + "]");
        String subtotal = subtotalExpression.evaluateFirst(peritoInsertRequest).getText();
        LOG.debug("### >> subtotal=[" + subtotal + "]");
        String iva = ivaExpression.evaluateFirst(peritoInsertRequest).getText();
        LOG.debug("### >> iva=[" + iva + "]");
        String total = totalExpression.evaluateFirst(peritoInsertRequest).getText();
        LOG.debug("### >> total=[" + total + "]");

        Perito perito = new Perito();
        perito.setTotalString(total);
        perito.setIvaString(iva);
        perito.setSubtotalString(subtotal);
        perito.setRfc(rfc);
        Long idPerito = (this.peritoApiService.save(perito) != null ? 
        		this.peritoApiService.save(perito).getIdPerito() : null);

        return createInsertReturnXML(idPerito);
    }

    /**
     * Genera el XML del resultado de la insercion del Perito.
     * @param idPerito El id del Perito generado por la secuencia de la BD.
     * @return Retorna el elemento XML con el resultado de la insercion del Perito.
     */
    private Element createInsertReturnXML(Long idPerito) {
        Element peritoResponse = new Element("peritoInsertResponse", NS);
        Element peritoElement = new Element("perito", NS);
        peritoResponse.addContent(peritoElement);
        peritoElement.addContent(createElement("idPerito", idPerito.toString()));

        return peritoResponse;
    }

    /**
     * Atiende las operaciones que necesitan eliminar un Perito.
     * @param PeritoDeleteRequest La peticion de eliminar un Perito.
     * @return El mensaje XML con el resultado de la eliminacion del Perito.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "peritoDeleteRequest")
    @ResponsePayload
    public Element handlePeritoDeleteRequest(@RequestPayload Element peritoDeleteRequest) 
           throws Exception {
        LOG.info("### PeritoServiceEndpoint.handlePeritoDeleteRequest...");
        String idPerito = idPeritoExpression.evaluateFirst(peritoDeleteRequest).getText();
        LOG.debug("### >> idPerito=[" + idPerito + "]");

        this.peritoApiService.borrar(new Long(idPerito));

        return createDeleteReturnXML(idPerito);
    }

    /**
     * Genera el XML del resultado de la eliminacion del Perito.
     * @param idPerito El id del Perito eliminado.
     * @return Retorna el elemento XML con el resultado de la eliminacion del Perito.
     */
    private Element createDeleteReturnXML(String idPerito) {
        Element peritoResponse = new Element("peritoDeleteResponse", NS);
        Element peritoElement = new Element("perito", NS);
        peritoResponse.addContent(peritoElement);
        peritoElement.addContent(createElement("resultado", "perito [" + idPerito + "] eliminado."));

        return peritoResponse;
    }

    /**
     * Atiende las operaciones que necesitan actualizar datos de un Perito.
     * @param PeritoUpdateRequest La peticion de actualizar los datos de un Perito e 
     *                                   incluye los datos del Perito.
     * @return El mensaje XML con el resultado de la actalizacion del Perito.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "peritoUpdateRequest")
    @ResponsePayload
    public Element handlePeritoUpdateRequest(@RequestPayload Element peritoUpdateRequest) 
           throws Exception {
        LOG.info("### PeritoServiceEndpoint.handlePeritoUpdateRequest...");
        String idPerito = idPeritoExpression.evaluateFirst(peritoUpdateRequest).getText();
        LOG.debug("### >> idPerito=[" + idPerito + "]");
        String rfc = rfcExpression.evaluateFirst(peritoUpdateRequest).getText();
        LOG.debug("### >> rfc=[" + rfc + "]");
        String subtotal = subtotalExpression.evaluateFirst(peritoUpdateRequest).getText();
        LOG.debug("### >> subtotal=[" + subtotal + "]");
        String iva = ivaExpression.evaluateFirst(peritoUpdateRequest).getText();
        LOG.debug("### >> iva=[" + iva + "]");
        String total = totalExpression.evaluateFirst(peritoUpdateRequest).getText();
        LOG.debug("### >> total=[" + total + "]");

        Perito perito = new Perito();
        if (idPerito != null) {
            perito.setIdPerito(new Long(idPerito));
        }
        if (rfc != null) {
            perito.setRfc(rfc);
        }
        if (subtotal != null) {
        	perito.setSubtotalString(subtotal);
        }
        if (iva != null) {
        	perito.setIvaString(iva);
        }
        if (total != null) {
        	perito.setTotalString(total);
        }

        perito = this.peritoApiService.actualizar(perito);

        return createUpdateReturnXML(idPerito);
    }

    /**
     * Genera el XML del resultado de la actualizacion del Perito.
     * @param idPerito El id del Perito actualizado.
     * @return Retorna el elemento XML con el resultado de la actualizacion del Perito.
     */
    private Element createUpdateReturnXML(String idPerito) {
        Element peritoResponse = new Element("peritoUpdateResponse", NS);
        Element peritoElement = new Element("perito", NS);
        peritoResponse.addContent(peritoElement);
        peritoElement.addContent(createElement("resultado", "perito [" + idPerito + "] actualizado."));

        return peritoResponse;
    }

}
