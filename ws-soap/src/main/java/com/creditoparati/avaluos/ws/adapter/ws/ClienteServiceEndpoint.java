/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.adapter.ws;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.creditoparati.avaluos.ws.api.ClienteService;
import com.creditoparati.avaluos.ws.domain.Cliente;

/**
 * Web Service de Cliente.
 * @author Edgar Deloera
 */
@Endpoint
public class ClienteServiceEndpoint {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ClienteServiceEndpoint.class);

    private static final String NAMESPACE_URI = "http://sat.gob.mx/contrib/schemas";

    private XPathExpression<Element> idClienteExpression;

    private XPathExpression<Element> nombreExpression;

    private XPathExpression<Element> rfcExpression;

    private XPathExpression<Element> tipoExpression;

    private XPathExpression<Element> cuentaExpression;

    private XPathExpression<Element> emailExpression;
    
    private ClienteService clienteApiService;

    private static Namespace NS = Namespace.getNamespace("cliente", NAMESPACE_URI);

    @Autowired
    public ClienteServiceEndpoint(ClienteService clienteApiService) throws JDOMException {
        this.clienteApiService = clienteApiService;
        XPathFactory xPathFactory = XPathFactory.instance();
        idClienteExpression = xPathFactory.compile("//contrib:idCliente", Filters.element(), null, NS);
        nombreExpression = xPathFactory.compile("//contrib:nombre", Filters.element(), null, NS);
        rfcExpression = xPathFactory.compile("//contrib:rfc", Filters.element(), null, NS);
        tipoExpression = xPathFactory.compile("//contrib:tipo", Filters.element(), null, NS);
        cuentaExpression = xPathFactory.compile("//contrib:cuenta", Filters.element(), null, NS);
        emailExpression = xPathFactory.compile("//contrib:email", Filters.element(), null, NS);
    }

    /**
     * Atiende las operaciones que necesitan el detalle de un Cliente.
     * @param ClienteDetailRequest La peticion de detalle de Cliente.
     * @return El mensaje XML con el detalle del Cliente.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "clienteDetailRequest")
    @ResponsePayload
    public Element handleClienteDetailRequest(@RequestPayload Element clienteDetailRequest) 
           throws Exception {
        LOG.info("### ClienteServiceEndpoint.handleClienteDetailRequest...");
        String idCliente = idClienteExpression.evaluateFirst(clienteDetailRequest).getText();
        LOG.debug("### >> idCliente=[" + idCliente + "]");

        Cliente cliente = null;
        cliente = this.clienteApiService.findCliente(new Long(idCliente));

        return createDetailReturnXML(cliente);
    }

    /**
     * Genera el XML del detalle del Cliente.
     * @param contrib El Cliente para obtener el detalle.
     * @return Retorna el elemento XML con el detalle del Cliente.
     */
    private Element createDetailReturnXML(Cliente cliente) {
        Element clienteResponse = new Element("clienteDetailResponse", NS);
        Element clienteElement = new Element("cliente", NS);
        clienteResponse.addContent(clienteElement);
        clienteElement.addContent(createElement("id", cliente.getIdCliente().toString()));
        clienteElement.addContent(createElement("nombre", cliente.getNombre()));
        clienteElement.addContent(createElement("rfc", cliente.getRfc()));
        clienteElement.addContent(createElement("tipo", cliente.getTipo()));
        clienteElement.addContent(createElement("email", cliente.getEmail()));
        clienteElement.addContent(createElement("cuenta", cliente.getCuenta()));

        return clienteResponse;
    }

    /**
     * Genera un elemento a adicionar en otro elemento contenedor. 
     * @param elementName El nombre del elemento a generar.
     * @param elementValue El valor del elemento a incluir.
     * @return El elemento para ser almacenado en otro. 
     */
    private Element createElement(String elementName, String elementValue) {

        Element element = new Element(elementName, NS);
        element.setText(elementValue);
        return element;
    }

    /**
     * Atiende las operaciones que necesitan insertar un Cliente.
     * @param ClienteInsertRequest La peticion de insertar el detalle con los datos del
     *                                   Cliente.
     * @return El mensaje XML con el resultado de la insercion del Cliente.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "clienteInsertRequest")
    @ResponsePayload
    public Element handleClienteInsertRequest(@RequestPayload Element clienteInsertRequest) 
           throws Exception {
        LOG.info("### ClienteServiceEndpoint.handleClienteInsertRequest...");
        String nombre = nombreExpression.evaluateFirst(clienteInsertRequest).getText();
        LOG.debug("### >> nombre=[" + nombre + "]");
        String rfc = rfcExpression.evaluateFirst(clienteInsertRequest).getText();
        LOG.debug("### >> rfc=[" + rfc + "]");
        String tipo = tipoExpression.evaluateFirst(clienteInsertRequest).getText();
        LOG.debug("### >> tipo=[" + tipo + "]");
        String cuenta = cuentaExpression.evaluateFirst(clienteInsertRequest).getText();
        LOG.debug("### >> cuenta=[" + cuenta + "]");
        String email = emailExpression.evaluateFirst(clienteInsertRequest).getText();
        LOG.debug("### >> email=[" + email + "]");

        
        Cliente cliente = new Cliente();
        cliente.setTipo(tipo);
        cliente.setNombre(nombre);
        cliente.setRfc(rfc);
        cliente.setCuenta(cuenta);
        cliente.setEmail(email);
        Long idCliente = (this.clienteApiService.save(cliente) != null ? 
        		this.clienteApiService.save(cliente).getIdCliente() : null);

        return createInsertReturnXML(idCliente);
    }

    /**
     * Genera el XML del resultado de la insercion del Cliente.
     * @param idCliente El id del Cliente generado por la secuencia de la BD.
     * @return Retorna el elemento XML con el resultado de la insercion del Cliente.
     */
    private Element createInsertReturnXML(Long idCliente) {
        Element clienteResponse = new Element("clienteInsertResponse", NS);
        Element cliente = new Element("cliente", NS);
        clienteResponse.addContent(cliente);
        cliente.addContent(createElement("idCliente", idCliente.toString()));

        return clienteResponse;
    }

    /**
     * Atiende las operaciones que necesitan eliminar un Cliente.
     * @param ClienteDeleteRequest La peticion de eliminar un Cliente.
     * @return El mensaje XML con el resultado de la eliminacion del Cliente.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "clienteDeleteRequest")
    @ResponsePayload
    public Element handleClienteDeleteRequest(@RequestPayload Element clienteDeleteRequest) 
           throws Exception {
        LOG.info("### ClienteServiceEndpoint.handleClienteDeleteRequest...");
        String idCliente = idClienteExpression.evaluateFirst(clienteDeleteRequest).getText();
        LOG.debug("### >> idCliente=[" + idCliente + "]");

        this.clienteApiService.borrar(new Long(idCliente));

        return createDeleteReturnXML(idCliente);
    }

    /**
     * Genera el XML del resultado de la eliminacion del Cliente.
     * @param idCliente El id del Cliente eliminado.
     * @return Retorna el elemento XML con el resultado de la eliminacion del Cliente.
     */
    private Element createDeleteReturnXML(String idCliente) {
        Element clienteResponse = new Element("clienteDeleteResponse", NS);
        Element cliente = new Element("Cliente", NS);
        clienteResponse.addContent(cliente);
        cliente.addContent(createElement("resultado", "cliente [" + idCliente + "] eliminado."));

        return clienteResponse;
    }

    /**
     * Atiende las operaciones que necesitan actualizar datos de un Cliente.
     * @param ClienteUpdateRequest La peticion de actualizar los datos de un Cliente e 
     *                                   incluye los datos del Cliente.
     * @return El mensaje XML con el resultado de la actalizacion del Cliente.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "clienteUpdateRequest")
    @ResponsePayload
    public Element handleClienteUpdateRequest(@RequestPayload Element clienteUpdateRequest) 
           throws Exception {
        LOG.info("### ClienteServiceEndpoint.handleClienteUpdateRequest...");
        String idCliente = idClienteExpression.evaluateFirst(clienteUpdateRequest).getText();
        LOG.debug("### >> idCliente=[" + idCliente + "]");
        String nombre = nombreExpression.evaluateFirst(clienteUpdateRequest).getText();
        LOG.debug("### >> nombre=[" + nombre + "]");
        String rfc = rfcExpression.evaluateFirst(clienteUpdateRequest).getText();
        LOG.debug("### >> rfc=[" + rfc + "]");
        String tipo = tipoExpression.evaluateFirst(clienteUpdateRequest).getText();
        LOG.debug("### >> tipo=[" + tipo + "]");
        String cuenta = cuentaExpression.evaluateFirst(clienteUpdateRequest).getText();
        LOG.debug("### >> cuenta=[" + cuenta + "]");
        String email = emailExpression.evaluateFirst(clienteUpdateRequest).getText();
        LOG.debug("### >> email=[" + email + "]");
        
        Cliente cliente = new Cliente();
        if (idCliente != null) {
        	cliente.setIdCliente(new Long(idCliente));
        }
        if (nombre != null) {
        	cliente.setNombre(nombre);
        }
        if (rfc != null) {
        	cliente.setRfc(rfc);
        }
        if (tipo != null) {
        	cliente.setTipo(tipo);
        }

        cliente = this.clienteApiService.actualizar(cliente);

        return createUpdateReturnXML(idCliente);
    }

    /**
     * Genera el XML del resultado de la actualizacion del Cliente.
     * @param idCliente El id del Cliente actualizado.
     * @return Retorna el elemento XML con el resultado de la actualizacion del Cliente.
     */
    private Element createUpdateReturnXML(String idCliente) {
        Element clienteResponse = new Element("clienteUpdateResponse", NS);
        Element cliente = new Element("cliente", NS);
        clienteResponse.addContent(cliente);
        cliente.addContent(createElement("resultado", "cliente [" + idCliente + "] actualizado."));

        return clienteResponse;
    }

}
