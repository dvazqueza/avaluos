/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.adapter.ws;

import com.creditoparati.avaluos.ws.api.ControladorService;
import com.creditoparati.avaluos.ws.domain.Controlador;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * Web Service de Controlador.
 * @author Edgar Deloera
 */
@Endpoint
public class ControladorServiceEndpoint {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ControladorServiceEndpoint.class);

    private static final String NAMESPACE_URI = "http://sat.gob.mx/controlador/schemas";

    private XPathExpression<Element> idControladorExpression;

    private XPathExpression<Element> subtotalExpression;

    private XPathExpression<Element> ivaExpression;

    private XPathExpression<Element> totalExpression;

    private XPathExpression<Element> rfcExpression;
    
    private ControladorService controladorApiService;

    private static Namespace NS = Namespace.getNamespace("controlador", NAMESPACE_URI);

    @Autowired
    public ControladorServiceEndpoint(ControladorService controladorApiService) throws JDOMException {
        this.controladorApiService = controladorApiService;
        XPathFactory xPathFactory = XPathFactory.instance();
        idControladorExpression = xPathFactory.compile("//controlador:idControlador", Filters.element(), null, NS);
        rfcExpression = xPathFactory.compile("//controlador:rfc", Filters.element(), null, NS);
        subtotalExpression = xPathFactory.compile("//controlador:subtotal", Filters.element(), null, NS);
        ivaExpression = xPathFactory.compile("//controlador:iva", Filters.element(), null, NS);
        totalExpression = xPathFactory.compile("//controlador:total", Filters.element(), null, NS);
    }

    /**
     * Atiende las operaciones que necesitan el detalle de un Controlador.
     * @param ControladorDetailRequest La peticion de detalle de Controlador.
     * @return El mensaje XML con el detalle del Controlador.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "controladorDetailRequest")
    @ResponsePayload
    public Element handleControladorDetailRequest(@RequestPayload Element controladorDetailRequest) 
           throws Exception {
        LOG.info("### ControladorServiceEndpoint.handleControladorDetailRequest...");
        String idControlador = idControladorExpression.evaluateFirst(controladorDetailRequest).getText();
        LOG.debug("### >> idControlador=[" + idControlador + "]");

        Controlador controlador = null;
        controlador = this.controladorApiService.findControlador(new Long(idControlador));

        return createDetailReturnXML(controlador);
    }

    /**
     * Genera el XML del detalle del Controlador.
     * @param controlador El Controlador para obtener el detalle.
     * @return Retorna el elemento XML con el detalle del Controlador.
     */
    private Element createDetailReturnXML(Controlador controlador) {
        Element controladorResponse = new Element("controladorDetailResponse", NS);
        Element controladorElement = new Element("controlador", NS);
        controladorResponse.addContent(controladorElement);
        controladorElement.addContent(createElement("id", controlador.getIdControlador().toString()));
        controladorElement.addContent(createElement("rfc", controlador.getRfc()));
        controladorElement.addContent(createElement("subtotal", controlador.getSubtotal().toString()));
        controladorElement.addContent(createElement("iva", controlador.getIva().toString()));
        controladorElement.addContent(createElement("total", controlador.getTotal().toString()));

        return controladorResponse;
    }

    /**
     * Genera un elemento a adicionar en otro elemento contenedor. 
     * @param elementName El nombre del elemento a generar.
     * @param elementValue El valor del elemento a incluir.
     * @return El elemento para ser almacenado en otro. 
     */
    private Element createElement(String elementName, String elementValue) {

        Element element = new Element(elementName, NS);
        element.setText(elementValue);
        return element;
    }

    /**
     * Atiende las operaciones que necesitan insertar un Controlador.
     * @param ControladorInsertRequest La peticion de insertar el detalle con los datos del
     *                                   Controlador.
     * @return El mensaje XML con el resultado de la insercion del Controlador.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "controladorInsertRequest")
    @ResponsePayload
    public Element handleControladorInsertRequest(@RequestPayload Element controladorInsertRequest) 
           throws Exception {
        LOG.info("### ControladorServiceEndpoint.handleControladorInsertRequest...");
        String rfc = rfcExpression.evaluateFirst(controladorInsertRequest).getText();
        LOG.debug("### >> rfc=[" + rfc + "]");
        String subtotal = subtotalExpression.evaluateFirst(controladorInsertRequest).getText();
        LOG.debug("### >> subtotal=[" + subtotal + "]");
        String iva = ivaExpression.evaluateFirst(controladorInsertRequest).getText();
        LOG.debug("### >> iva=[" + iva + "]");
        String total = totalExpression.evaluateFirst(controladorInsertRequest).getText();
        LOG.debug("### >> total=[" + total + "]");

        Controlador controlador = new Controlador();
        controlador.setTotalString(total);
        controlador.setIvaString(iva);
        controlador.setSubtotalString(subtotal);
        controlador.setRfc(rfc);
        Long idControlador = (this.controladorApiService.save(controlador) != null ? 
        		this.controladorApiService.save(controlador).getIdControlador() : null);

        return createInsertReturnXML(idControlador);
    }

    /**
     * Genera el XML del resultado de la insercion del Controlador.
     * @param idControlador El id del Controlador generado por la secuencia de la BD.
     * @return Retorna el elemento XML con el resultado de la insercion del Controlador.
     */
    private Element createInsertReturnXML(Long idControlador) {
        Element controladorResponse = new Element("controladorInsertResponse", NS);
        Element controladorElement = new Element("controlador", NS);
        controladorResponse.addContent(controladorElement);
        controladorElement.addContent(createElement("idControlador", idControlador.toString()));

        return controladorResponse;
    }

    /**
     * Atiende las operaciones que necesitan eliminar un Controlador.
     * @param ControladorDeleteRequest La peticion de eliminar un Controlador.
     * @return El mensaje XML con el resultado de la eliminacion del Controlador.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "controladorDeleteRequest")
    @ResponsePayload
    public Element handleControladorDeleteRequest(@RequestPayload Element controladorDeleteRequest) 
           throws Exception {
        LOG.info("### ControladorServiceEndpoint.handleControladorDeleteRequest...");
        String idControlador = idControladorExpression.evaluateFirst(controladorDeleteRequest).getText();
        LOG.debug("### >> idControlador=[" + idControlador + "]");

        this.controladorApiService.borrar(new Long(idControlador));

        return createDeleteReturnXML(idControlador);
    }

    /**
     * Genera el XML del resultado de la eliminacion del Controlador.
     * @param idControlador El id del Controlador eliminado.
     * @return Retorna el elemento XML con el resultado de la eliminacion del Controlador.
     */
    private Element createDeleteReturnXML(String idControlador) {
        Element controladorResponse = new Element("controladorDeleteResponse", NS);
        Element controladorElement = new Element("controlador", NS);
        controladorResponse.addContent(controladorElement);
        controladorElement.addContent(createElement("resultado", "controlador [" + idControlador + "] eliminado."));

        return controladorResponse;
    }

    /**
     * Atiende las operaciones que necesitan actualizar datos de un Controlador.
     * @param ControladorUpdateRequest La peticion de actualizar los datos de un Controlador e 
     *                                   incluye los datos del Controlador.
     * @return El mensaje XML con el resultado de la actalizacion del Controlador.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "controladorUpdateRequest")
    @ResponsePayload
    public Element handleControladorUpdateRequest(@RequestPayload Element controladorUpdateRequest) 
           throws Exception {
        LOG.info("### ControladorServiceEndpoint.handleControladorUpdateRequest...");
        String idControlador = idControladorExpression.evaluateFirst(controladorUpdateRequest).getText();
        LOG.debug("### >> idControlador=[" + idControlador + "]");
        String rfc = rfcExpression.evaluateFirst(controladorUpdateRequest).getText();
        LOG.debug("### >> rfc=[" + rfc + "]");
        String subtotal = subtotalExpression.evaluateFirst(controladorUpdateRequest).getText();
        LOG.debug("### >> subtotal=[" + subtotal + "]");
        String iva = ivaExpression.evaluateFirst(controladorUpdateRequest).getText();
        LOG.debug("### >> iva=[" + iva + "]");
        String total = totalExpression.evaluateFirst(controladorUpdateRequest).getText();
        LOG.debug("### >> total=[" + total + "]");

        Controlador controlador = new Controlador();
        if (idControlador != null) {
            controlador.setIdControlador(new Long(idControlador));
        }

        if (rfc != null) {
            controlador.setRfc(rfc);
        }
        if (subtotal != null) {
            controlador.setSubtotalString(subtotal);
        }
        if (iva != null) {
            controlador.setIvaString(iva);
        }
        if (total != null) {
            controlador.setTotalString(total);
        }
        controlador = this.controladorApiService.actualizar(controlador);

        return createUpdateReturnXML(idControlador);
    }

    /**
     * Genera el XML del resultado de la actualizacion del Controlador.
     * @param idControlador El id del Controlador actualizado.
     * @return Retorna el elemento XML con el resultado de la actualizacion del Controlador.
     */
    private Element createUpdateReturnXML(String idControlador) {
        Element controladorResponse = new Element("controladorUpdateResponse", NS);
        Element controladorElement = new Element("controlador", NS);
        controladorResponse.addContent(controladorElement);
        controladorElement.addContent(createElement("resultado", "controlador [" + idControlador + "] actualizado."));

        return controladorResponse;
    }

}
