/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.adapter.ws;

import com.creditoparati.avaluos.ws.api.ServicioService;
import com.creditoparati.avaluos.ws.domain.Servicio;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * Web Service de Servicio.
 * @author Edgar Deloera
 */
@Endpoint
public class ServicioServiceEndpoint {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ServicioServiceEndpoint.class);

    private static final String NAMESPACE_URI = "http://sat.gob.mx/servicio/schemas";

    private XPathExpression<Element> idServicioExpression;

    private XPathExpression<Element> estatusExpression;

    private XPathExpression<Element> fecGeneracionExpression;

    private XPathExpression<Element> subtotalExpression;
    
    private XPathExpression<Element> ivaExpression;
    
    private XPathExpression<Element> totalExpression;
    
    private XPathExpression<Element> referenciaExpression;
    
    private XPathExpression<Element> remesaExpression;
    
    private XPathExpression<Element> servicioExpression;
    
    private XPathExpression<Element> sucursalExpression;
    
    private XPathExpression<Element> tipoExpression;

    
    
    private ServicioService servicioApiService;

    private static Namespace NS = Namespace.getNamespace("servicio", NAMESPACE_URI);

    @Autowired
    public ServicioServiceEndpoint(ServicioService servicioApiService) throws JDOMException {
        this.servicioApiService = servicioApiService;
        XPathFactory xPathFactory = XPathFactory.instance();
        idServicioExpression = xPathFactory.compile("//servicio:idServicio", Filters.element(), null, NS);
        fecGeneracionExpression = xPathFactory.compile("//servicio:fecGeneracion", Filters.element(), null, NS);
        subtotalExpression = xPathFactory.compile("//servicio:subtotal", Filters.element(), null, NS);
        ivaExpression = xPathFactory.compile("//servicio:iva", Filters.element(), null, NS);
        totalExpression = xPathFactory.compile("//servicio:total", Filters.element(), null, NS);
        estatusExpression = xPathFactory.compile("//servicio:estatus", Filters.element(), null, NS);
        referenciaExpression = xPathFactory.compile("//servicio:referencia", Filters.element(), null, NS);
        remesaExpression = xPathFactory.compile("//servicio:remesa", Filters.element(), null, NS);
        servicioExpression = xPathFactory.compile("//servicio:servicio", Filters.element(), null, NS);
        tipoExpression = xPathFactory.compile("//servicio:tipo", Filters.element(), null, NS);
    }

    /**
     * Atiende las operaciones que necesitan el detalle de un Servicio.
     * @param ServicioDetailRequest La peticion de detalle de Servicio.
     * @return El mensaje XML con el detalle del Servicio.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "servicioDetailRequest")
    @ResponsePayload
    public Element handleServicioDetailRequest(@RequestPayload Element servicioDetailRequest) 
           throws Exception {
        LOG.info("### ServicioServiceEndpoint.handleServicioDetailRequest...");
        String idServicio = idServicioExpression.evaluateFirst(servicioDetailRequest).getText();
        LOG.debug("### >> idServicio=[" + idServicio + "]");

        Servicio servicio = null;
        servicio = this.servicioApiService.findServicio(new Long(idServicio));

        return createDetailReturnXML(servicio);
    }

    /**
     * Genera el XML del detalle del Servicio.
     * @param servicio El Servicio para obtener el detalle.
     * @return Retorna el elemento XML con el detalle del Servicio.
     */
    private Element createDetailReturnXML(Servicio servicio) {
        Element servicioResponse = new Element("servicioDetailResponse", NS);
        Element servicioElement = new Element("servicio", NS);
        servicioResponse.addContent(servicioElement);
        servicioElement.addContent(createElement("id", servicio.getIdServicio().toString()));
        servicioElement.addContent(createElement("estatus", servicio.getEstatus()));
        servicioElement.addContent(createElement("fecGeneracion", servicio.getFecGeneracion().toString()));
        servicioElement.addContent(createElement("subtotal", servicio.getSubtotal().toString()));
        servicioElement.addContent(createElement("iva", servicio.getIva().toString()));
        servicioElement.addContent(createElement("total", servicio.getTotal().toString()));
        servicioElement.addContent(createElement("referencia", servicio.getReferencia()));
        servicioElement.addContent(createElement("remesa", servicio.getRemesa()));
        servicioElement.addContent(createElement("servicio", servicio.getServicio()));
        servicioElement.addContent(createElement("sucursal", servicio.getSucursal()));
        servicioElement.addContent(createElement("tipo", servicio.getTipo()));
        
        return servicioResponse;
    }

    /**
     * Genera un elemento a adicionar en otro elemento contenedor. 
     * @param elementName El nombre del elemento a generar.
     * @param elementValue El valor del elemento a incluir.
     * @return El elemento para ser almacenado en otro. 
     */
    private Element createElement(String elementName, String elementValue) {

        Element element = new Element(elementName, NS);
        element.setText(elementValue);
        return element;
    }

    /**
     * Atiende las operaciones que necesitan insertar un Servicio.
     * @param ServicioInsertRequest La peticion de insertar el detalle con los datos del
     *                                   Servicio.
     * @return El mensaje XML con el resultado de la insercion del Servicio.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "servicioInsertRequest")
    @ResponsePayload
    public Element handleServicioInsertRequest(@RequestPayload Element servicioInsertRequest) 
           throws Exception {
        LOG.info("### ServicioServiceEndpoint.handleServicioInsertRequest...");
        String estatus = estatusExpression.evaluateFirst(servicioInsertRequest).getText();
        LOG.debug("### >> estatus=[" + estatus + "]");
        String fecGeneracion = fecGeneracionExpression.evaluateFirst(servicioInsertRequest).getText();
        LOG.debug("### >> fecGeneracion=[" + fecGeneracion + "]");
        String subtotal = subtotalExpression.evaluateFirst(servicioInsertRequest).getText();
        LOG.debug("### >> subtotal=[" + subtotal + "]");
        String iva = ivaExpression.evaluateFirst(servicioInsertRequest).getText();
        LOG.debug("### >> iva=[" + iva + "]");
        String total = totalExpression.evaluateFirst(servicioInsertRequest).getText();
        LOG.debug("### >> total=[" + total + "]");

        String referencia = referenciaExpression.evaluateFirst(servicioInsertRequest).getText();
        LOG.debug("### >> referencia=[" + referencia + "]");
        String remesa = remesaExpression.evaluateFirst(servicioInsertRequest).getText();
        LOG.debug("### >> remesa=[" + remesa + "]");
        String servicioTag = servicioExpression.evaluateFirst(servicioInsertRequest).getText();
        LOG.debug("### >> servicio=[" + servicioTag + "]");
        String sucursal = sucursalExpression.evaluateFirst(servicioInsertRequest).getText();
        LOG.debug("### >> sucursal=[" + sucursal + "]");
        String tipo = tipoExpression.evaluateFirst(servicioInsertRequest).getText();
        LOG.debug("### >> tipo=[" + tipo + "]");

        Servicio servicio = new Servicio();
        servicio.setFecGeneracionString(fecGeneracion);
        servicio.setSubtotalString(subtotal);
        servicio.setIvaString(iva);
        servicio.setTotalString(total);
        servicio.setEstatus(estatus);
        servicio.setReferencia(referencia);
        servicio.setRemesa(remesa);
        servicio.setServicio(servicioTag);
        servicio.setTipo(tipo);
        Long idServicio = (this.servicioApiService.save(servicio) != null ? 
        		this.servicioApiService.save(servicio).getIdServicio() : null);

        return createInsertReturnXML(idServicio);
    }

    /**
     * Genera el XML del resultado de la insercion del Servicio.
     * @param idServicio El id del Servicio generado por la secuencia de la BD.
     * @return Retorna el elemento XML con el resultado de la insercion del Servicio.
     */
    private Element createInsertReturnXML(Long idServicio) {
        Element servicioResponse = new Element("servicioInsertResponse", NS);
        Element servicioElement = new Element("servicio", NS);
        servicioResponse.addContent(servicioElement);
        servicioElement.addContent(createElement("idServicio", idServicio.toString()));

        return servicioResponse;
    }

    /**
     * Atiende las operaciones que necesitan eliminar un Servicio.
     * @param ServicioDeleteRequest La peticion de eliminar un Servicio.
     * @return El mensaje XML con el resultado de la eliminacion del Servicio.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "servicioDeleteRequest")
    @ResponsePayload
    public Element handleServicioDeleteRequest(@RequestPayload Element servicioDeleteRequest) 
           throws Exception {
        LOG.info("### ServicioServiceEndpoint.handleServicioDeleteRequest...");
        String idServicio = idServicioExpression.evaluateFirst(servicioDeleteRequest).getText();
        LOG.debug("### >> idServicio=[" + idServicio + "]");

        this.servicioApiService.borrar(new Long(idServicio));

        return createDeleteReturnXML(idServicio);
    }

    /**
     * Genera el XML del resultado de la eliminacion del Servicio.
     * @param idServicio El id del Servicio eliminado.
     * @return Retorna el elemento XML con el resultado de la eliminacion del Servicio.
     */
    private Element createDeleteReturnXML(String idServicio) {
        Element servicioResponse = new Element("servicioDeleteResponse", NS);
        Element servicioElement = new Element("servicio", NS);
        servicioResponse.addContent(servicioElement);
        servicioElement.addContent(createElement("resultado", "servicio [" + idServicio + "] eliminado."));

        return servicioResponse;
    }

    /**
     * Atiende las operaciones que necesitan actualizar datos de un Servicio.
     * @param ServicioUpdateRequest La peticion de actualizar los datos de un Servicio e 
     *                                   incluye los datos del Servicio.
     * @return El mensaje XML con el resultado de la actalizacion del Servicio.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "servicioUpdateRequest")
    @ResponsePayload
    public Element handleServicioUpdateRequest(@RequestPayload Element servicioUpdateRequest) 
           throws Exception {
        LOG.info("### ServicioServiceEndpoint.handleServicioUpdateRequest...");
        String idServicio = idServicioExpression.evaluateFirst(servicioUpdateRequest).getText();
        LOG.debug("### >> idServicio=[" + idServicio + "]");
        String fecGeneracion = fecGeneracionExpression.evaluateFirst(servicioUpdateRequest).getText();
        LOG.debug("### >> fecGeneracion=[" + fecGeneracion + "]");
        String subtotal = subtotalExpression.evaluateFirst(servicioUpdateRequest).getText();
        LOG.debug("### >> subtotal=[" + subtotal + "]");
        String iva = ivaExpression.evaluateFirst(servicioUpdateRequest).getText();
        LOG.debug("### >> iva=[" + iva + "]");
        String total = totalExpression.evaluateFirst(servicioUpdateRequest).getText();
        LOG.debug("### >> total=[" + total + "]");
        String estatus = estatusExpression.evaluateFirst(servicioUpdateRequest).getText();
        LOG.debug("### >> estatus=[" + estatus + "]");
        String referencia = referenciaExpression.evaluateFirst(servicioUpdateRequest).getText();
        LOG.debug("### >> referencia=[" + referencia + "]");
        String remesa = remesaExpression.evaluateFirst(servicioUpdateRequest).getText();
        LOG.debug("### >> remesa=[" + remesa + "]");
        String servicioTag = servicioExpression.evaluateFirst(servicioUpdateRequest).getText();
        LOG.debug("### >> servicio=[" + servicioTag + "]");
        String sucursal = sucursalExpression.evaluateFirst(servicioUpdateRequest).getText();
        LOG.debug("### >> sucursal=[" + sucursal + "]");
        String tipo = tipoExpression.evaluateFirst(servicioUpdateRequest).getText();
        LOG.debug("### >> tipo=[" + tipo + "]");
        
        Servicio servicio = new Servicio();
        if (idServicio != null) {
            servicio.setIdServicio(new Long(idServicio));
        }
        if (fecGeneracion != null) {
            servicio.setFecGeneracionString(fecGeneracion);
        }
        if (subtotal != null) {
            servicio.setSubtotalString(subtotal);
        }
        if (iva != null) {
            servicio.setIvaString(iva);
        }
        if (total != null) {
            servicio.setTotalString(total);
        }
        if (estatus != null) {
            servicio.setEstatus(estatus);
        }
        if (referencia != null) {
            servicio.setReferencia(referencia);
        }
        if (remesa != null) {
            servicio.setRemesa(remesa);
        }
        if (servicioTag != null) {
            servicio.setServicio(servicioTag);
        }
        if (sucursal != null) {
            servicio.setSucursal(sucursal);
        }
        if (tipo != null) {
            servicio.setTipo(tipo);
        }

        
        servicio = this.servicioApiService.actualizar(servicio);

        return createUpdateReturnXML(idServicio);
    }

    /**
     * Genera el XML del resultado de la actualizacion del Servicio.
     * @param idServicio El id del Servicio actualizado.
     * @return Retorna el elemento XML con el resultado de la actualizacion del Servicio.
     */
    private Element createUpdateReturnXML(String idServicio) {
        Element servicioResponse = new Element("servicioUpdateResponse", NS);
        Element Servicio = new Element("servicio", NS);
        servicioResponse.addContent(Servicio);
        Servicio.addContent(createElement("resultado", "servicio [" + idServicio + "] actualizado."));

        return servicioResponse;
    }

}
