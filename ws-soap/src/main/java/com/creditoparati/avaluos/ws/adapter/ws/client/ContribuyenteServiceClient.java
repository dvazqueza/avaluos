/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.adapter.ws.client;

import javax.xml.soap.SAAJResult;
import javax.xml.soap.SOAPException;
import javax.xml.transform.Source;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.xml.transform.StringSource;

/**
 * Clase cliente del WS ContribuyenteServiceEndpoint.
 * @author Edgar Deloera.
 */
@Component
public class ContribuyenteServiceClient {

	@Autowired
	WebServiceTemplate contribuyenteServiceWSClient;

	public String invokeContribuyenteDetailServiceAndGetASuccessResponse(String idContribuyente) throws SOAPException {

        // Armado del XML del request.
        Source requestPayload = new StringSource(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
            "<sch:ContribuyenteDetailRequest xmlns:sch=\"http://sat.gob.mx/contrib/schemas\">" +
            "   <sch:idContribuyente>" + idContribuyente + "</sch:idContribuyente>" +
            "</sch:ContribuyenteDetailRequest>"
        );

        SAAJResult saajRes = new SAAJResult();

		this.contribuyenteServiceWSClient.sendSourceAndReceiveToResult(requestPayload, saajRes);

        return saajRes.getResult().getTextContent();
	}

}











