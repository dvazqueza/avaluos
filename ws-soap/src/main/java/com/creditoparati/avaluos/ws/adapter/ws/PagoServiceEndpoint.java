/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.adapter.ws;

import com.creditoparati.avaluos.ws.api.PagoService;
import com.creditoparati.avaluos.ws.domain.Pago;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * Web Service de Pago.
 * @author Edgar Deloera
 */
@Endpoint
public class PagoServiceEndpoint {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(PagoServiceEndpoint.class);

    private static final String NAMESPACE_URI = "http://sat.gob.mx/contrib/schemas";

    private XPathExpression<Element> idPagoExpression;

    private XPathExpression<Element> estatusExpression;

    private XPathExpression<Element> facturaExpression;

    private XPathExpression<Element> fechaExpression;

    private XPathExpression<Element> referenciaExpression;

    private XPathExpression<Element> remesaExpression;

    private XPathExpression<Element> subtotalExpression;
    
    private XPathExpression<Element> ivaExpression;
    
    private XPathExpression<Element> totalExpression;

    private XPathExpression<Element> servicioExpression;
    
    private XPathExpression<Element> sucursalExpression;

    private XPathExpression<Element> tipoExpression;

    private PagoService pagoApiService;

    private static Namespace NS = Namespace.getNamespace("pago", NAMESPACE_URI);

    @Autowired
    public PagoServiceEndpoint(PagoService pagoApiService) throws JDOMException {
        this.pagoApiService = pagoApiService;
        XPathFactory xPathFactory = XPathFactory.instance();
        idPagoExpression = xPathFactory.compile("//pago:idPago", Filters.element(), null, NS);
        estatusExpression = xPathFactory.compile("//pago:estatus", Filters.element(), null, NS);
        facturaExpression = xPathFactory.compile("//pago:factura", Filters.element(), null, NS);
        fechaExpression = xPathFactory.compile("//pago:fecha", Filters.element(), null, NS);
        referenciaExpression = xPathFactory.compile("//pago:referencia", Filters.element(), null, NS);
        remesaExpression = xPathFactory.compile("//pago:remesa", Filters.element(), null, NS);
        subtotalExpression = xPathFactory.compile("//pago:subtotal", Filters.element(), null, NS);
        ivaExpression = xPathFactory.compile("//pago:iva", Filters.element(), null, NS);
        totalExpression = xPathFactory.compile("//pago:total", Filters.element(), null, NS);
        servicioExpression = xPathFactory.compile("//pago:servicio", Filters.element(), null, NS);
        sucursalExpression = xPathFactory.compile("//pago:sucursal", Filters.element(), null, NS);
        tipoExpression = xPathFactory.compile("//pago:tipo", Filters.element(), null, NS);
    }

    /**
     * Atiende las operaciones que necesitan el detalle de un Pago.
     * @param PagoDetailRequest La peticion de detalle de Pago.
     * @return El mensaje XML con el detalle del Pago.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "pagoDetailRequest")
    @ResponsePayload
    public Element handlePagoDetailRequest(@RequestPayload Element pagoDetailRequest) 
           throws Exception {
        LOG.info("### PagoServiceEndpoint.handlePagoDetailRequest...");
        String idPago = idPagoExpression.evaluateFirst(pagoDetailRequest).getText();
        LOG.debug("### >> idPago=[" + idPago + "]");

        Pago pago = null;
        pago = this.pagoApiService.findPago(new Long(idPago));

        return createDetailReturnXML(pago);
    }

    /**
     * Genera el XML del detalle del Pago.
     * @param contrib El Pago para obtener el detalle.
     * @return Retorna el elemento XML con el detalle del Pago.
     */
    private Element createDetailReturnXML(Pago pago) {
        Element pagoResponse = new Element("PagoDetailResponse", NS);
        Element pagoElement = new Element("Pago", NS);
        pagoResponse.addContent(pagoElement);
        pagoElement.addContent(createElement("id", pago.getIdPago().toString()));
        pagoElement.addContent(createElement("estatus", pago.getEstatus()));
        pagoElement.addContent(createElement("factura", pago.getFactura()));
        pagoElement.addContent(createElement("fecha", pago.getFecha().toString()));
        pagoElement.addContent(createElement("subtotal", pago.getSubtotal().toString()));
        pagoElement.addContent(createElement("iva", pago.getIva().toString()));
        pagoElement.addContent(createElement("total", pago.getTotal().toString()));
        pagoElement.addContent(createElement("referencia", pago.getReferencia()));
        pagoElement.addContent(createElement("remesa", pago.getRemesa()));
        pagoElement.addContent(createElement("servicio", pago.getServicio()));
        pagoElement.addContent(createElement("sucursal", pago.getSucursal()));
        pagoElement.addContent(createElement("tipo", pago.getTipo()));

        return pagoResponse;
    }

    /**
     * Genera un elemento a adicionar en otro elemento contenedor. 
     * @param elementName El nombre del elemento a generar.
     * @param elementValue El valor del elemento a incluir.
     * @return El elemento para ser almacenado en otro. 
     */
    private Element createElement(String elementName, String elementValue) {

        Element element = new Element(elementName, NS);
        element.setText(elementValue);
        return element;
    }

    /**
     * Atiende las operaciones que necesitan insertar un Pago.
     * @param PagoInsertRequest La peticion de insertar el detalle con los datos del
     *                                   Pago.
     * @return El mensaje XML con el resultado de la insercion del Pago.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "pagoInsertRequest")
    @ResponsePayload
    public Element handlePagoInsertRequest(@RequestPayload Element pagoInsertRequest) 
           throws Exception {
        LOG.info("### PagoServiceEndpoint.handlePagoInsertRequest...");
        String estatus = estatusExpression.evaluateFirst(pagoInsertRequest).getText();
        LOG.debug("### >> estatus=[" + estatus + "]");
        String factura = facturaExpression.evaluateFirst(pagoInsertRequest).getText();
        LOG.debug("### >> factura=[" + factura + "]");
        String fecha = fechaExpression.evaluateFirst(pagoInsertRequest).getText();
        LOG.debug("### >> fecha=[" + fecha + "]");
        String subtotal = subtotalExpression.evaluateFirst(pagoInsertRequest).getText();
        LOG.debug("### >> subtotal=[" + subtotal + "]");
        String iva = ivaExpression.evaluateFirst(pagoInsertRequest).getText();
        LOG.debug("### >> iva=[" + iva + "]");
        String total = totalExpression.evaluateFirst(pagoInsertRequest).getText();
        LOG.debug("### >> total=[" + total + "]");
        String referencia = referenciaExpression.evaluateFirst(pagoInsertRequest).getText();
        LOG.debug("### >> referencia=[" + referencia + "]");        
        String remesa = remesaExpression.evaluateFirst(pagoInsertRequest).getText();
        LOG.debug("### >> remesa=[" + remesa + "]");        
        String servicio = servicioExpression.evaluateFirst(pagoInsertRequest).getText();
        LOG.debug("### >> servicio=[" + servicio + "]");        
        String sucursal = sucursalExpression.evaluateFirst(pagoInsertRequest).getText();
        LOG.debug("### >> sucursal=[" + sucursal + "]");        
        String tipo = tipoExpression.evaluateFirst(pagoInsertRequest).getText();
        LOG.debug("### >> tipo=[" + tipo + "]");
        
        Pago pago = new Pago();
        pago.setFechaString(fecha);
        pago.setEstatus(estatus);
        pago.setFactura(factura);
        pago.setSubtotalString(subtotal);
        pago.setIvaString(iva);
        pago.setTotalString(total);
        pago.setReferencia(referencia);
        pago.setRemesa(remesa);
        pago.setServicio(servicio);
        pago.setSucursal(sucursal);
        Long idPago = (this.pagoApiService.save(pago) != null ? 
        		this.pagoApiService.save(pago).getIdPago() : null);

        return createInsertReturnXML(idPago);
    }

    /**
     * Genera el XML del resultado de la insercion del Pago.
     * @param idPago El id del Pago generado por la secuencia de la BD.
     * @return Retorna el elemento XML con el resultado de la insercion del Pago.
     */
    private Element createInsertReturnXML(Long idPago) {
        Element pagoResponse = new Element("pagoInsertResponse", NS);
        Element pagoElement = new Element("pago", NS);
        pagoResponse.addContent(pagoElement);
        pagoElement.addContent(createElement("idPago", idPago.toString()));

        return pagoResponse;
    }

    /**
     * Atiende las operaciones que necesitan eliminar un Pago.
     * @param PagoDeleteRequest La peticion de eliminar un Pago.
     * @return El mensaje XML con el resultado de la eliminacion del Pago.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "pagoDeleteRequest")
    @ResponsePayload
    public Element handlePagoDeleteRequest(@RequestPayload Element pagoDeleteRequest) 
           throws Exception {
        LOG.info("### PagoServiceEndpoint.handlePagoDeleteRequest...");
        String idPago = idPagoExpression.evaluateFirst(pagoDeleteRequest).getText();
        LOG.debug("### >> idPago=[" + idPago + "]");

        this.pagoApiService.borrar(new Long(idPago));

        return createDeleteReturnXML(idPago);
    }

    /**
     * Genera el XML del resultado de la eliminacion del Pago.
     * @param idPago El id del Pago eliminado.
     * @return Retorna el elemento XML con el resultado de la eliminacion del Pago.
     */
    private Element createDeleteReturnXML(String idPago) {
        Element pagoResponse = new Element("pagoDeleteResponse", NS);
        Element pagoElement = new Element("pago", NS);
        pagoResponse.addContent(pagoElement);
        pagoElement.addContent(createElement("resultado", "pago [" + idPago + "] eliminado."));

        return pagoResponse;
    }

    /**
     * Atiende las operaciones que necesitan actualizar datos de un Pago.
     * @param PagoUpdateRequest La peticion de actualizar los datos de un Pago e 
     *                                   incluye los datos del Pago.
     * @return El mensaje XML con el resultado de la actalizacion del Pago.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "pagoUpdateRequest")
    @ResponsePayload
    public Element handlePagoUpdateRequest(@RequestPayload Element pagoUpdateRequest) 
           throws Exception {
        LOG.info("### PagoServiceEndpoint.handlePagoUpdateRequest...");
        String idPago = idPagoExpression.evaluateFirst(pagoUpdateRequest).getText();
        LOG.debug("### >> idPago=[" + idPago + "]");
        String iva = ivaExpression.evaluateFirst(pagoUpdateRequest).getText();
        LOG.debug("### >> iva=[" + iva + "]");
        String subtotal = subtotalExpression.evaluateFirst(pagoUpdateRequest).getText();
        LOG.debug("### >> subtotal =[" + subtotal + "]");
        String total = totalExpression.evaluateFirst(pagoUpdateRequest).getText();
        LOG.debug("### >> total=[" + total + "]");
        String fecha = fechaExpression.evaluateFirst(pagoUpdateRequest).getText();
        LOG.debug("### >> fecha=[" + fecha + "]");

        String estatus = estatusExpression.evaluateFirst(pagoUpdateRequest).getText();
        LOG.debug("### >> estatus=[" + estatus + "]");
        String factura = facturaExpression.evaluateFirst(pagoUpdateRequest).getText();
        LOG.debug("### >> factura=[" + factura + "]");
        String referencia = referenciaExpression.evaluateFirst(pagoUpdateRequest).getText();
        LOG.debug("### >> referencia=[" + referencia + "]");
        String remesa = remesaExpression.evaluateFirst(pagoUpdateRequest).getText();
        LOG.debug("### >> remesa=[" + remesa + "]");
        String servicio = servicioExpression.evaluateFirst(pagoUpdateRequest).getText();
        LOG.debug("### >> servicio=[" + servicio + "]");
        String sucursal = sucursalExpression.evaluateFirst(pagoUpdateRequest).getText();
        LOG.debug("### >> sucursal=[" + sucursal + "]");
        String tipo = tipoExpression.evaluateFirst(pagoUpdateRequest).getText();
        LOG.debug("### >> tipo=[" + tipo + "]");

        
        Pago pago = new Pago();
        if (idPago != null) {
            pago.setIdPago(new Long(idPago));
        }
        if (estatus != null) {
            pago.setEstatus(estatus);
        }
        if (factura != null) {
            pago.setFactura(factura);
        }
        if (referencia != null) {
            pago.setReferencia(referencia);
        }
        if (remesa != null) {
            pago.setRemesa(remesa);
        }
        if (servicio != null) {
            pago.setServicio(servicio);
        }
        if (sucursal != null) {
            pago.setSucursal(sucursal);
        }
        if (tipo != null) {
            pago.setTipo(tipo);
        }
        if (fecha != null) {
            pago.setFechaString(fecha);
        }
        if (subtotal != null) {
            pago.setSubtotalString(subtotal);
        }
        if (iva != null) {
            pago.setIvaString(iva);
        }
        if (total != null) {
            pago.setTotalString(total);
        }

        pago = this.pagoApiService.actualizar(pago);

        return createUpdateReturnXML(idPago);
    }

    /**
     * Genera el XML del resultado de la actualizacion del Pago.
     * @param idPago El id del Pago actualizado.
     * @return Retorna el elemento XML con el resultado de la actualizacion del Pago.
     */
    private Element createUpdateReturnXML(String idPago) {
        Element pagoResponse = new Element("pagoUpdateResponse", NS);
        Element pagoElement = new Element("pago", NS);
        pagoResponse.addContent(pagoElement);
        pagoElement.addContent(createElement("resultado", "pago [" + idPago + "] actualizado."));

        return pagoResponse;
    }

}
