/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.adapter.ws;

import java.util.Date;
import java.util.List;

import com.creditoparati.avaluos.ws.api.ControlServicioService;
import com.creditoparati.avaluos.ws.domain.Cliente;
import com.creditoparati.avaluos.ws.domain.ControlServicio;
import com.creditoparati.avaluos.ws.domain.Controlador;
import com.creditoparati.avaluos.ws.domain.Domicilio;
import com.creditoparati.avaluos.ws.domain.Perito;
import com.creditoparati.avaluos.ws.domain.Servicio;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * Web Service de ControlServicio.
 * @author Edgar Deloera
 */
@Endpoint
public class ControlServicioServiceEndpoint {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ControlServicioServiceEndpoint.class);

    private static final String NAMESPACE_URI = "http://www.creditoparati.com.mx/hr/schemas";

    private XPathExpression<Element> idControlServicioExpression;
    
    private ControlServicioService controlServicioApiService;

    private static Namespace NS = Namespace.getNamespace("sch", NAMESPACE_URI);

    @Autowired
    public ControlServicioServiceEndpoint(ControlServicioService controlServicioApiService) throws JDOMException {
        this.controlServicioApiService = controlServicioApiService;
        XPathFactory xPathFactory = XPathFactory.instance();
        idControlServicioExpression = xPathFactory.compile("//sch:id_control_servicio", Filters.element(), null, NS);
    }

    /**
     * Genera un elemento a adicionar en otro elemento contenedor. 
     * @param elementName El nombre del elemento a generar.
     * @param elementValue El valor del elemento a incluir.
     * @return El elemento para ser almacenado en otro. 
     */
    private Element createElement(String elementName, String elementValue) {

        Element element = new Element(elementName, NS);
        element.setText(elementValue);
        return element;
    }

    /**
     * Atiende las operaciones que necesitan insertar un ControlServicio.
     * @param ControlServicioInsertRequest La peticion de insertar el detalle con los datos del
     *                                   ControlServicio.
     * @return El mensaje XML con el resultado de la insercion del ControlServicio.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ControlServicioRequest")
    @ResponsePayload
    public Element handleControlServicioInsertRequest(@RequestPayload Element controlServicioInsertRequest) 
           throws Exception {
        LOG.info("### ControlServicioServiceEndpoint.handleControlServicioInsertRequest...");

        ControlServicio controlServicio = new ControlServicio();
        controlServicio.setFechaCreacion(new Date());
                
        List<Element> elementos  = controlServicioInsertRequest.getChildren();
        
        for (Element elemento :elementos) {
        	String nameElemento = elemento.getName();
        	LOG.debug("### nameElemento = [" + nameElemento + "]");
        	
        	List<Element> childElementos = elemento.getChildren();
        	if("cliente".equals(nameElemento)) {
        		Cliente cliente = new Cliente();
        		cliente.setControlServicio(controlServicio);
        		controlServicio.setCliente(cliente);
        		cliente = clienteAddValues(cliente, childElementos);
        	}
        	if("domicilio".equals(nameElemento)) {
        		Domicilio domicilio = new Domicilio();
        		domicilio.setControlServicio(controlServicio);
        		controlServicio.setDomicilio(domicilio);
        		domicilio = domicilioAddValues(domicilio, childElementos);
        	}
        	if("servicio".equals(nameElemento)) {
        		Servicio servicio = new Servicio();
        		servicio.setControlServicio(controlServicio);
        		controlServicio.setServicio(servicio);
        		servicio = servicioAddValues(servicio, childElementos);
        	}
        	if("perito".equals(nameElemento)) {
        		Perito perito = new Perito();
        		perito.setControlServicio(controlServicio);
        		controlServicio.setPerito(perito);
        		perito = peritoAddValues(perito, childElementos);
        	}
        	if("controlador".equals(nameElemento)) {
        		Controlador controlador = new Controlador();
        		controlador.setControlServicio(controlServicio);
        		controlServicio.setControlador(controlador);
        		controlador = controladorAddValues(controlador, childElementos);
        	}
        }
        
        controlServicio =  this.controlServicioApiService.save(controlServicio);
        LOG.info("### controlServicio  = [" + controlServicio + "] con id = [" + controlServicio.getIdControlServicio() + "]");
        
        return createInsertReturnXML(controlServicio);
    }

    /**
     * Genera el XML del resultado de la insercion del ControlServicio.
     * @param idControlServicio El id del ControlServicio generado por la secuencia de la BD.
     * @return Retorna el elemento XML con el resultado de la insercion del ControlServicio.
     */
    private Element createInsertReturnXML(ControlServicio controlServicio) {
        Element controlServicioResponse = new Element("ControlServicioResponse", NS);
        controlServicioResponse.addContent(createElement("id_control_servicio", controlServicio.getIdControlServicio().toString()));
        controlServicioResponse.addContent(createElement("fecha_creacion", controlServicio.getFechaCreacionString()));
        controlServicioResponse.addContent(createServiceDetailReturnXML(controlServicio.getServicio()));
        
        return controlServicioResponse;
    }
    
    
    private Cliente clienteAddValues(Cliente cliente, List<Element> elementos) {
    	if(!elementos.isEmpty()) {
    		for(Element elemento : elementos) {
    			String nombreElemento = elemento.getName();
    			LOG.debug("### cliente.nombreElemento = [" + nombreElemento + "]");
    			if("nombre".equals(nombreElemento)) {
    				cliente.setNombre(elemento.getValue());
    			}
    			if("rfc".equals(nombreElemento)) {
    				cliente.setRfc(elemento.getValue());
    			}
    			if("tipo".equals(nombreElemento)) {
    				cliente.setTipo(elemento.getValue());
    			}
    			if("cuenta".equals(nombreElemento)) {
    				cliente.setCuenta(elemento.getValue());
    			}
    			if("email".equals(nombreElemento)) {
    				cliente.setEmail(elemento.getValue());
    			}
    		}
    	}
    	return cliente;
    }

    private Domicilio domicilioAddValues(Domicilio domicilio, List<Element> elementos) {
    	if(!elementos.isEmpty()) {
    		for(Element elemento : elementos) {
    			String nombreElemento = elemento.getName();
    			LOG.debug("### domicilio.nombreElemento = [" + nombreElemento + "]");
    			if("calle".equals(nombreElemento)) {
    				domicilio.setCalle(elemento.getValue());
    			}
    			if("colonia".equals(nombreElemento)) {
    				domicilio.setColonia(elemento.getValue());
    			}
    			if("municipio".equals(nombreElemento)) {
    				domicilio.setMunicipio(elemento.getValue());
    			}
    			if("estado".equals(nombreElemento)) {
    				domicilio.setEstado(elemento.getValue());
    			}
    			if("cp".equals(nombreElemento)) {
    				domicilio.setCp(elemento.getValue());
    			}
    			if("num_exterior".equals(nombreElemento)) {
    				domicilio.setNumExterior(elemento.getValue());
    			}
    			if("num_interior".equals(nombreElemento)) {
    				domicilio.setNumInterior(elemento.getValue());
    			}
    		}
    	}
    	return domicilio;
    }

    private Servicio servicioAddValues(Servicio servicio, List<Element> elementos) {
    	if(!elementos.isEmpty()) {
    		for(Element elemento : elementos) {
    			String nombreElemento = elemento.getName();
    			LOG.debug("### servicio.nombreElemento = [" + nombreElemento + "]");
    			if("estatus".equals(nombreElemento)) {
    				servicio.setEstatus(elemento.getValue());
    			}
    			if("tipo".equals(nombreElemento)) {
    				servicio.setTipo(elemento.getValue());
    			}
    			if("remesa".equals(nombreElemento)) {
    				servicio.setRemesa(elemento.getValue());
    			}
    			if("referencia".equals(nombreElemento)) {
    				servicio.setReferencia(elemento.getValue());
    			}
    			if("servicio".equals(nombreElemento)) {
    				servicio.setServicio(elemento.getValue());
    			}
    			if("sucursal".equals(nombreElemento)) {
    				servicio.setSucursal(elemento.getValue());
    			}
    			if("fec_generacion".equals(nombreElemento)) {
    				servicio.setFecGeneracionString(elemento.getValue());
    			}
    			if("subtotal".equals(nombreElemento)) {
    				servicio.setSubtotalString(elemento.getValue());
    			}
    			if("iva".equals(nombreElemento)) {
    				servicio.setIvaString(elemento.getValue());
    			}
    			if("total".equals(nombreElemento)) {
    				servicio.setTotalString(elemento.getValue());
    			}
    		}
    	}
    	return servicio;
    }

    private Perito peritoAddValues(Perito perito, List<Element> elementos) {
    	if(!elementos.isEmpty()) {
    		for(Element elemento : elementos) {
    			String nombreElemento = elemento.getName();
    			LOG.debug("### perito.nombreElemento = [" + nombreElemento + "]");
    			if("rfc".equals(nombreElemento)) {
    				perito.setRfc(elemento.getValue());
    			}
    			if("subtotal".equals(nombreElemento)) {
    				perito.setSubtotalString(elemento.getValue());
    			}
    			if("iva".equals(nombreElemento)) {
    				perito.setIvaString(elemento.getValue());
    			}
    			if("total".equals(nombreElemento)) {
    				perito.setTotalString(elemento.getValue());
    			}
    		}
    	}
    	return perito;
    }

    private Controlador controladorAddValues(Controlador controlador, List<Element> elementos) {
    	if(!elementos.isEmpty()) {
    		for(Element elemento : elementos) {
    			String nombreElemento = elemento.getName();
    			LOG.debug("### controlador.nombreElemento = [" + nombreElemento + "]");
    			if("rfc".equals(nombreElemento)) {
    				controlador.setRfc(elemento.getValue());
    			}
    			if("subtotal".equals(nombreElemento)) {
    				controlador.setSubtotalString(elemento.getValue());
    			}
    			if("iva".equals(nombreElemento)) {
    				controlador.setIvaString(elemento.getValue());
    			}
    			if("total".equals(nombreElemento)) {
    				controlador.setTotalString(elemento.getValue());
    			}
    		}
    	}
    	return controlador;
    }

    
    /**
     * Genera el XML del detalle del Servicio.
     * @param servicio El Servicio para obtener el detalle.
     * @return Retorna el elemento XML con el detalle del Servicio.
     */
    private Element createServiceDetailReturnXML(Servicio servicio) {
        
    	Element servicioElement = new Element("servicio", NS);
        servicioElement.addContent(createElement("id", servicio.getIdServicio().toString()));
        servicioElement.addContent(createElement("estatus", servicio.getEstatus()));
        servicioElement.addContent(createElement("fec_generacion", servicio.getFecGeneracion().toString()));
        servicioElement.addContent(createElement("subtotal", servicio.getSubtotal().toString()));
        servicioElement.addContent(createElement("iva", servicio.getIva().toString()));
        servicioElement.addContent(createElement("total", servicio.getTotal().toString()));
        servicioElement.addContent(createElement("referencia", servicio.getReferencia()));
        servicioElement.addContent(createElement("remesa", servicio.getRemesa()));
        servicioElement.addContent(createElement("servicio", servicio.getServicio()));
        servicioElement.addContent(createElement("sucursal", servicio.getSucursal()));
        servicioElement.addContent(createElement("tipo", servicio.getTipo()));
       
        return servicioElement;
    }
}
