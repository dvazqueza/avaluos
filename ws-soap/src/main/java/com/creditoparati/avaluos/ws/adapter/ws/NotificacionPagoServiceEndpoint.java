/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.adapter.ws;

import com.creditoparati.avaluos.ws.api.NotificacionPagoService;
import com.creditoparati.avaluos.ws.domain.NotificacionPago;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * Web Service de NotificacionPago.
 * @author Edgar Deloera
 */
@Endpoint
public class NotificacionPagoServiceEndpoint {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(NotificacionPagoServiceEndpoint.class);

    private static final String NAMESPACE_URI = "http://sat.gob.mx/notificacionPago/schemas";

    private XPathExpression<Element> idNotificacionPagoExpression;

    private XPathExpression<Element> idPagoExpression;

    private NotificacionPagoService notificacionPagoApiService;

    private static Namespace NS = Namespace.getNamespace("notificacionPago", NAMESPACE_URI);

    @Autowired
    public NotificacionPagoServiceEndpoint(NotificacionPagoService notificacionPagoApiService) throws JDOMException {
        this.notificacionPagoApiService = notificacionPagoApiService;
        XPathFactory xPathFactory = XPathFactory.instance();
        idNotificacionPagoExpression = xPathFactory.compile("//notificacionPago:idNotificacionPago", Filters.element(), null, NS);
        idPagoExpression = xPathFactory.compile("//notificacionPago:idPago", Filters.element(), null, NS);

    }

    /**
     * Atiende las operaciones que necesitan el detalle de un NotificacionPago.
     * @param NotificacionPagoDetailRequest La peticion de detalle de NotificacionPago.
     * @return El mensaje XML con el detalle del NotificacionPago.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "notificacionPagoDetailRequest")
    @ResponsePayload
    public Element handleNotificacionPagoDetailRequest(@RequestPayload Element notificacionPagoDetailRequest) 
           throws Exception {
        LOG.info("### NotificacionPagoServiceEndpoint.handleNotificacionPagoDetailRequest...");
        String idNotificacionPago = idNotificacionPagoExpression.evaluateFirst(notificacionPagoDetailRequest).getText();
        LOG.debug("### >> idNotificacionPago=[" + idNotificacionPago + "]");

        NotificacionPago notificacionPago = null;
        notificacionPago = this.notificacionPagoApiService.findNotificacionPago(new Long(idNotificacionPago));

        return createDetailReturnXML(notificacionPago);
    }

    /**
     * Genera el XML del detalle del NotificacionPago.
     * @param notificacionPago El NotificacionPago para obtener el detalle.
     * @return Retorna el elemento XML con el detalle del NotificacionPago.
     */
    private Element createDetailReturnXML(NotificacionPago notificacionPago) {
        Element notificacionPagoResponse = new Element("notificacionPagoDetailResponse", NS);
        Element notificacionPagoElement = new Element("notificacionPago", NS);
        notificacionPagoResponse.addContent(notificacionPagoElement);
        notificacionPagoElement.addContent(createElement("id", notificacionPago.getIdNotificacionPago().toString()));
        // notificacionPagoElement.addContent(createElement("pago", notificacionPago.getPago().toString()));

        return notificacionPagoResponse;
    }

    /**
     * Genera un elemento a adicionar en otro elemento contenedor. 
     * @param elementName El nombre del elemento a generar.
     * @param elementValue El valor del elemento a incluir.
     * @return El elemento para ser almacenado en otro. 
     */
    private Element createElement(String elementName, String elementValue) {

        Element element = new Element(elementName, NS);
        element.setText(elementValue);
        return element;
    }

    /**
     * Atiende las operaciones que necesitan insertar un NotificacionPago.
     * @param NotificacionPagoInsertRequest La peticion de insertar el detalle con los datos del
     *                                   NotificacionPago.
     * @return El mensaje XML con el resultado de la insercion del NotificacionPago.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "notificacionPagoInsertRequest")
    @ResponsePayload
    public Element handleNotificacionPagoInsertRequest(@RequestPayload Element notificacionPagoInsertRequest) 
           throws Exception {
        LOG.info("### NotificacionPagoServiceEndpoint.handleNotificacionPagoInsertRequest...");
        String idPago = idPagoExpression.evaluateFirst(notificacionPagoInsertRequest).getText();
        LOG.debug("### >> idPago=[" + idPago + "]");

        NotificacionPago notificacionPago = new NotificacionPago();
        // notificacionPago.setIdPagoString(idPago);

        Long idNotificacionPago = (this.notificacionPagoApiService.save(notificacionPago) != null ? 
        		this.notificacionPagoApiService.save(notificacionPago).getIdNotificacionPago() : null);

        return createInsertReturnXML(idNotificacionPago);
    }

    /**
     * Genera el XML del resultado de la insercion del NotificacionPago.
     * @param idNotificacionPago El id del NotificacionPago generado por la secuencia de la BD.
     * @return Retorna el elemento XML con el resultado de la insercion del NotificacionPago.
     */
    private Element createInsertReturnXML(Long idNotificacionPago) {
        Element notificacionPagoResponse = new Element("notificacionPagoInsertResponse", NS);
        Element notificacionPagoElement = new Element("notificacionPago", NS);
        notificacionPagoResponse.addContent(notificacionPagoElement);
        notificacionPagoElement.addContent(createElement("idNotificacionPago", idNotificacionPago.toString()));

        return notificacionPagoResponse;
    }

    /**
     * Atiende las operaciones que necesitan eliminar un NotificacionPago.
     * @param NotificacionPagoDeleteRequest La peticion de eliminar un NotificacionPago.
     * @return El mensaje XML con el resultado de la eliminacion del NotificacionPago.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "notificacionPagoDeleteRequest")
    @ResponsePayload
    public Element handleNotificacionPagoDeleteRequest(@RequestPayload Element notificacionPagoDeleteRequest) 
           throws Exception {
        LOG.info("### NotificacionPagoServiceEndpoint.handleNotificacionPagoDeleteRequest...");
        String idNotificacionPago = idNotificacionPagoExpression.evaluateFirst(notificacionPagoDeleteRequest).getText();
        LOG.debug("### >> idNotificacionPago=[" + idNotificacionPago + "]");

        this.notificacionPagoApiService.borrar(new Long(idNotificacionPago));

        return createDeleteReturnXML(idNotificacionPago);
    }

    /**
     * Genera el XML del resultado de la eliminacion del NotificacionPago.
     * @param idNotificacionPago El id del NotificacionPago eliminado.
     * @return Retorna el elemento XML con el resultado de la eliminacion del NotificacionPago.
     */
    private Element createDeleteReturnXML(String idNotificacionPago) {
        Element notificacionPagoResponse = new Element("notificacionPagoDeleteResponse", NS);
        Element notificacionPagoElement = new Element("notificacionPago", NS);
        notificacionPagoResponse.addContent(notificacionPagoElement);
        notificacionPagoElement.addContent(createElement("resultado", "notificacionPago [" + idNotificacionPago + "] eliminado."));

        return notificacionPagoResponse;
    }

    /**
     * Atiende las operaciones que necesitan actualizar datos de un NotificacionPago.
     * @param NotificacionPagoUpdateRequest La peticion de actualizar los datos de un NotificacionPago e 
     *                                   incluye los datos del NotificacionPago.
     * @return El mensaje XML con el resultado de la actalizacion del NotificacionPago.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "notificacionPagoUpdateRequest")
    @ResponsePayload
    public Element handleNotificacionPagoUpdateRequest(@RequestPayload Element notificacionPagoUpdateRequest) 
           throws Exception {
        LOG.info("### NotificacionPagoServiceEndpoint.handleNotificacionPagoUpdateRequest...");
        String idNotificacionPago = idNotificacionPagoExpression.evaluateFirst(notificacionPagoUpdateRequest).getText();
        LOG.debug("### >> idNotificacionPago=[" + idNotificacionPago + "]");
        String idPago = idPagoExpression.evaluateFirst(notificacionPagoUpdateRequest).getText();
        LOG.debug("### >> idPago =[" + idPago + "]");


        NotificacionPago notificacionPago = new NotificacionPago();
        if (idNotificacionPago != null) {
            notificacionPago.setIdNotificacionPago(new Long(idNotificacionPago));
        }
//        if (idPago != null) {
//            notificacionPago.setIdPagoString(idPago);
//        }

        notificacionPago = this.notificacionPagoApiService.actualizar(notificacionPago);

        return createUpdateReturnXML(idNotificacionPago);
    }

    /**
     * Genera el XML del resultado de la actualizacion del NotificacionPago.
     * @param idNotificacionPago El id del NotificacionPago actualizado.
     * @return Retorna el elemento XML con el resultado de la actualizacion del NotificacionPago.
     */
    private Element createUpdateReturnXML(String idNotificacionPago) {
        Element notificacionPagoResponse = new Element("notificacionPagoUpdateResponse", NS);
        Element notificacionPagoElement = new Element("notificacionPago", NS);
        notificacionPagoResponse.addContent(notificacionPagoElement);
        notificacionPagoElement.addContent(createElement("resultado", "notificacionPago [" + idNotificacionPago + "] actualizado."));

        return notificacionPagoResponse;
    }

}
