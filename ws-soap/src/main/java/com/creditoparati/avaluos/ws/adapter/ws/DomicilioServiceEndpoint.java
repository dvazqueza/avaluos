/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.adapter.ws;

import com.creditoparati.avaluos.ws.api.DomicilioService;
import com.creditoparati.avaluos.ws.domain.Domicilio;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * Web Service de Domicilio.
 * @author Edgar Deloera
 */
@Endpoint
public class DomicilioServiceEndpoint {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DomicilioServiceEndpoint.class);

    private static final String NAMESPACE_URI = "http://sat.gob.mx/contrib/schemas";

    private XPathExpression<Element> idDomicilioExpression;

    private XPathExpression<Element> calleExpression;

    private XPathExpression<Element> coloniaExpression;

    private XPathExpression<Element> estadoExpression;

    private XPathExpression<Element> cpExpression;

    private XPathExpression<Element> municipioExpression;

    private XPathExpression<Element> numExteriorExpression;
    
    private XPathExpression<Element> numInteriorExpression;
    
    private DomicilioService domicilioApiService;

    private static Namespace NS = Namespace.getNamespace("domicilio", NAMESPACE_URI);

    @Autowired
    public DomicilioServiceEndpoint(DomicilioService domicilioApiService) throws JDOMException {
        this.domicilioApiService = domicilioApiService;
        XPathFactory xPathFactory = XPathFactory.instance();
        idDomicilioExpression = xPathFactory.compile("//contrib:idDomicilio", Filters.element(), null, NS);
        calleExpression = xPathFactory.compile("//contrib:calle", Filters.element(), null, NS);
        coloniaExpression = xPathFactory.compile("//contrib:colonia", Filters.element(), null, NS);
        cpExpression = xPathFactory.compile("//contrib:cp", Filters.element(), null, NS);
        estadoExpression = xPathFactory.compile("//contrib:estado", Filters.element(), null, NS);
        municipioExpression = xPathFactory.compile("//contrib:municipio", Filters.element(), null, NS);
        numExteriorExpression = xPathFactory.compile("//contrib:num_exterior", Filters.element(), null, NS);
        numInteriorExpression = xPathFactory.compile("//contrib:num_interior", Filters.element(), null, NS);
    }

    /**
     * Atiende las operaciones que necesitan el detalle de un Domicilio.
     * @param DomicilioDetailRequest La peticion de detalle de Domicilio.
     * @return El mensaje XML con el detalle del Domicilio.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "domicilioDetailRequest")
    @ResponsePayload
    public Element handleDomicilioDetailRequest(@RequestPayload Element domicilioDetailRequest) 
           throws Exception {
        LOG.info("### DomicilioServiceEndpoint.handleDomicilioDetailRequest...");
        String idDomicilio = idDomicilioExpression.evaluateFirst(domicilioDetailRequest).getText();
        LOG.debug("### >> idDomicilio=[" + idDomicilio + "]");

        Domicilio domicilio = null;
        domicilio = this.domicilioApiService.findDomicilio(new Long(idDomicilio));

        return createDetailReturnXML(domicilio);
    }

    /**
     * Genera el XML del detalle del Domicilio.
     * @param contrib El Domicilio para obtener el detalle.
     * @return Retorna el elemento XML con el detalle del Domicilio.
     */
    private Element createDetailReturnXML(Domicilio domicilio) {
        Element domicilioResponse = new Element("domicilioDetailResponse", NS);
        Element domicilioElement = new Element("domicilio", NS);
        domicilioResponse.addContent(domicilioElement);
        domicilioElement.addContent(createElement("id", domicilio.getIdDomicilio().toString()));
        domicilioElement.addContent(createElement("calle", domicilio.getCalle()));
        domicilioElement.addContent(createElement("colonia", domicilio.getColonia()));
        domicilioElement.addContent(createElement("cp", domicilio.getCp().toString()));
        domicilioElement.addContent(createElement("estado", domicilio.getEstado().toString()));
        domicilioElement.addContent(createElement("municipio", domicilio.getMunicipio().toString()));
        domicilioElement.addContent(createElement("num_exterior", domicilio.getNumExterior().toString()));
        domicilioElement.addContent(createElement("num_interior", domicilio.getNumInterior().toString()));

        return domicilioResponse;
    }

    /**
     * Genera un elemento a adicionar en otro elemento contenedor. 
     * @param elementName El nombre del elemento a generar.
     * @param elementValue El valor del elemento a incluir.
     * @return El elemento para ser almacenado en otro. 
     */
    private Element createElement(String elementName, String elementValue) {

        Element element = new Element(elementName, NS);
        element.setText(elementValue);
        return element;
    }

    /**
     * Atiende las operaciones que necesitan insertar un Domicilio.
     * @param DomicilioInsertRequest La peticion de insertar el detalle con los datos del
     *                                   Domicilio.
     * @return El mensaje XML con el resultado de la insercion del Domicilio.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "domicilioInsertRequest")
    @ResponsePayload
    public Element handleDomicilioInsertRequest(@RequestPayload Element domicilioInsertRequest) 
           throws Exception {
        LOG.info("### DomicilioServiceEndpoint.handleDomicilioInsertRequest...");
        String calle = calleExpression.evaluateFirst(domicilioInsertRequest).getText();
        LOG.debug("### >> calle=[" + calle + "]");
        String colonia = coloniaExpression.evaluateFirst(domicilioInsertRequest).getText();
        LOG.debug("### >> colonia=[" + colonia + "]");
        String cp = cpExpression.evaluateFirst(domicilioInsertRequest).getText();
        LOG.debug("### >> cp=[" + cp + "]");
        String estado = estadoExpression.evaluateFirst(domicilioInsertRequest).getText();
        LOG.debug("### >> estado=[" + estado + "]");
        String municipio = municipioExpression.evaluateFirst(domicilioInsertRequest).getText();
        LOG.debug("### >> municipio=[" + municipio + "]");
        String numExterior = numExteriorExpression.evaluateFirst(domicilioInsertRequest).getText();
        LOG.debug("### >> numExterior=[" + numExterior + "]");
        String numInterior = numInteriorExpression.evaluateFirst(domicilioInsertRequest).getText();
        LOG.debug("### >> numInterior=[" + numInterior + "]");


        Domicilio domicilio = new Domicilio();
        domicilio.setNumInterior(numInterior);
        domicilio.setNumExterior(numExterior);
        domicilio.setMunicipio(municipio);
        domicilio.setEstado(estado);
        domicilio.setCp(cp);
        domicilio.setColonia(colonia);
        domicilio.setCalle(calle);
        Long idDomicilio = (this.domicilioApiService.save(domicilio) != null ? 
        		this.domicilioApiService.save(domicilio).getIdDomicilio() : null);

        return createInsertReturnXML(idDomicilio);
    }

    /**
     * Genera el XML del resultado de la insercion del Domicilio.
     * @param idDomicilio El id del Domicilio generado por la secuencia de la BD.
     * @return Retorna el elemento XML con el resultado de la insercion del Domicilio.
     */
    private Element createInsertReturnXML(Long idDomicilio) {
        Element domicilioResponse = new Element("domicilioInsertResponse", NS);
        Element domicilioElement = new Element("domicilio", NS);
        domicilioResponse.addContent(domicilioElement);
        domicilioElement.addContent(createElement("idDomicilio", idDomicilio.toString()));

        return domicilioResponse;
    }

    /**
     * Atiende las operaciones que necesitan eliminar un Domicilio.
     * @param DomicilioDeleteRequest La peticion de eliminar un Domicilio.
     * @return El mensaje XML con el resultado de la eliminacion del Domicilio.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "domicilioDeleteRequest")
    @ResponsePayload
    public Element handleDomicilioDeleteRequest(@RequestPayload Element domicilioDeleteRequest) 
           throws Exception {
        LOG.info("### DomicilioServiceEndpoint.handleDomicilioDeleteRequest...");
        String idDomicilio = idDomicilioExpression.evaluateFirst(domicilioDeleteRequest).getText();
        LOG.debug("### >> idDomicilio=[" + idDomicilio + "]");

        this.domicilioApiService.borrar(new Long(idDomicilio));

        return createDeleteReturnXML(idDomicilio);
    }

    /**
     * Genera el XML del resultado de la eliminacion del Domicilio.
     * @param idDomicilio El id del Domicilio eliminado.
     * @return Retorna el elemento XML con el resultado de la eliminacion del Domicilio.
     */
    private Element createDeleteReturnXML(String idDomicilio) {
        Element domicilioResponse = new Element("domicilioDeleteResponse", NS);
        Element domicilioElement = new Element("domicilio", NS);
        domicilioResponse .addContent(domicilioElement );
        domicilioElement .addContent(createElement("resultado", "domicilio [" + idDomicilio + "] eliminado."));

        return domicilioResponse ;
    }

    /**
     * Atiende las operaciones que necesitan actualizar datos de un Domicilio.
     * @param DomicilioUpdateRequest La peticion de actualizar los datos de un Domicilio e 
     *                                   incluye los datos del Domicilio.
     * @return El mensaje XML con el resultado de la actalizacion del Domicilio.
     * @throws Exception
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "domicilioUpdateRequest")
    @ResponsePayload
    public Element handleDomicilioUpdateRequest(@RequestPayload Element domicilioUpdateRequest) 
           throws Exception {
        LOG.info("### DomicilioServiceEndpoint.handleDomicilioUpdateRequest...");
        String idDomicilio = idDomicilioExpression.evaluateFirst(domicilioUpdateRequest).getText();
        LOG.debug("### >> idDomicilio=[" + idDomicilio + "]");
        String calle = calleExpression.evaluateFirst(domicilioUpdateRequest).getText();
        LOG.debug("### >> calle=[" + calle + "]");
        String colonia = coloniaExpression.evaluateFirst(domicilioUpdateRequest).getText();
        LOG.debug("### >> colonia=[" + colonia + "]");
        String cp = cpExpression.evaluateFirst(domicilioUpdateRequest).getText();
        LOG.debug("### >> cp=[" + cp + "]");
        String estado = estadoExpression.evaluateFirst(domicilioUpdateRequest).getText();
        LOG.debug("### >> estado=[" + estado + "]");
        String municipio = municipioExpression.evaluateFirst(domicilioUpdateRequest).getText();
        LOG.debug("### >> municipio=[" + municipio + "]");        
        String numExterior = numExteriorExpression.evaluateFirst(domicilioUpdateRequest).getText();
        LOG.debug("### >> numExterior=[" + numExterior + "]");
        String numInterior = numInteriorExpression.evaluateFirst(domicilioUpdateRequest).getText();
        LOG.debug("### >> numInterior=[" + numInterior + "]");

        Domicilio domicilio = new Domicilio();
        if (idDomicilio != null) {
        	domicilio.setIdDomicilio(new Long(idDomicilio));
        }
        if (calle != null) {
        	domicilio.setCalle(calle);
        }
        if (colonia != null) {
        	domicilio.setColonia(colonia);
        }
        if (cp != null) {
        	domicilio.setCp(cp);
        }
        if (estado != null) {
        	domicilio.setEstado(estado);
        }
        if (municipio != null) {
        	domicilio.setMunicipio(municipio);
        }
        if (numExterior != null) {
        	domicilio.setNumExterior(numExterior);
        }
        if (numInterior != null) {
        	domicilio.setNumInterior(numInterior);
        }

        domicilio = this.domicilioApiService.actualizar(domicilio);

        return createUpdateReturnXML(idDomicilio);
    }

    /**
     * Genera el XML del resultado de la actualizacion del Domicilio.
     * @param idDomicilio El id del Domicilio actualizado.
     * @return Retorna el elemento XML con el resultado de la actualizacion del Domicilio.
     */
    private Element createUpdateReturnXML(String idDomicilio) {
        Element domicilioResponse = new Element("domicilioUpdateResponse", NS);
        Element domicilioElement = new Element("domicilio", NS);
        domicilioResponse.addContent(domicilioElement);
        domicilioElement.addContent(createElement("resultado", "domicilio [" + idDomicilio + "] actualizado."));

        return domicilioResponse;
    }

}
