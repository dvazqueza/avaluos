/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.adapter.ws;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.transform.Source;

import com.creditoparati.avaluos.ws.adapter.ws.client.ControlServicioServiceClient;
import com.creditoparati.avaluos.ws.domain.Cliente;
import com.creditoparati.avaluos.ws.domain.ControlServicio;
import com.creditoparati.avaluos.ws.domain.Controlador;
import com.creditoparati.avaluos.ws.domain.Domicilio;
import com.creditoparati.avaluos.ws.domain.Perito;
import com.creditoparati.avaluos.ws.domain.Servicio;
import com.creditoparati.avaluos.ws.api.ControlServicioService;

import static org.hamcrest.CoreMatchers.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.test.client.MockWebServiceServer;
import org.springframework.xml.transform.StringSource;

import static org.springframework.ws.test.client.RequestMatchers.*;
import static org.springframework.ws.test.client.ResponseCreators.*;
import static org.hamcrest.CoreMatchers.containsString;

/**
 * Prueba de Integracion con un cliente de WS del Endpoint de contribuyentes.
 * @author Edgar Deloera.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "file:src/main/webapp/WEB-INF/spring-ws-servlet.xml",
        "classpath*:/META-INF/spring/appCtx-*.xml"
})
@ActiveProfiles({"local"})
public class ControlServicioServiceWSClientIntegrationTest {

    /***
     * Log de clase.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ControlServicioServiceWSClientIntegrationTest.class);

    /**
     * Inyeccion del servicio de negocio del contribuyente, auxiliar en la prueba.
     */
    @Autowired
    private ControlServicioService controlServicioService;

    /**
     * Inyeccion del servicio de negocio del contribuyente, auxiliar en la prueba.
     */
    @Autowired
    private WebServiceTemplate controlServicioServiceWSClient;

    /**
     * Mock server para conectar el web service template del contribuyente.
     */
    private MockWebServiceServer mockServer;

    /**
     * Cliente de WS ContribuyenteServiceEndpoint.
     */
    @Autowired
    private ControlServicioServiceClient controlServicioServiceClient;

    /**
     * Metodo que se ejecuta al inicio del test.
     * Inicializa el mock y asigna un valor al mapa de name spaces.
     */
    @Before
    public void createMockServer() {
        this.mockServer = MockWebServiceServer.createServer(this.controlServicioServiceWSClient);
    }

    /**
     * Test de la obtencion del detalle de un contribuyente.
     * @throws Exception
     */
    @Test
    public void debeDevolverControlServicioCuandoControlServicioDetailRequest() throws Exception {
        LOG.debug("### >> debeDevolverControlServicioCuandoControlServicioDetailRequest()...");

        // Insertar un contribuyente para la prueba de obtener el detalle de contribuyente.
        ControlServicio controlServicio = new ControlServicio();
        Cliente cliente = new Cliente();
		Domicilio domicilio = new Domicilio();
		Servicio servicio = new Servicio();
		Perito perito = new Perito();
		Controlador controlador = new Controlador();
		
		controlServicio.setFechaCreacion(new Date());
		controlServicio.setCliente(cliente);
		controlServicio.setDomicilio(domicilio);
		controlServicio.setServicio(servicio);
		controlServicio.setPerito(perito);
		controlServicio.setControlador(controlador);

		cliente.setControlServicio(controlServicio);
		domicilio.setControlServicio(controlServicio);
		servicio.setControlServicio(controlServicio);
		perito.setControlServicio(controlServicio);
		controlador.setControlServicio(controlServicio);
		
		cliente.setCuenta("0000");
		cliente.setEmail("aaaaa@mail.com");
		cliente.setNombre("Nombre Cliente");
		cliente.setRfc("AAAA000000XXX");
		cliente.setTipo("M");
		
		domicilio.setCalle("Calle");
		domicilio.setColonia("Colonia");
		domicilio.setCp("00000");
		domicilio.setEstado("09");
		domicilio.setMunicipio("014");
		domicilio.setNumExterior("701");
		domicilio.setNumInterior("304A");
		
		servicio.setEstatus("1");
		servicio.setFecGeneracion(new Date());
		servicio.setSubtotal(new BigDecimal(0));
		servicio.setIva(new BigDecimal(0));
		servicio.setTotal(new BigDecimal(0));
		servicio.setReferencia("1111111");
		servicio.setRemesa("100001");
		servicio.setServicio("100001");
		servicio.setSucursal("01");
		servicio.setTipo("MA");
		
		perito.setRfc("AAAA000000XXX");
		perito.setSubtotal(new BigDecimal(0));
		perito.setIva(new BigDecimal(0));
		perito.setTotal(new BigDecimal(0));

		controlador.setRfc("AAAA000000XXX");
		controlador.setSubtotal(new BigDecimal(0));
		controlador.setIva(new BigDecimal(0));
		controlador.setTotal(new BigDecimal(0));
        
        controlServicio = this.controlServicioService.save(controlServicio);
        LOG.debug("### IDCONT DETAIL=[" + controlServicio.getIdControlServicio() + "]");

        // Armado del XML del request.
        Source requestPayload = new StringSource(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
            "<sch:ContribuyenteDetailRequest xmlns:sch=\"http://sat.gob.mx/contrib/schemas\">" +
            "   <sch:idContribuyente>" + controlServicio.getIdControlServicio() + "</sch:idContribuyente>" +
            "</sch:ContribuyenteDetailRequest>"
        );

        // Declaracion del response esperado.
        Source responsePayload = new StringSource(
                "<contrib:contribuyenteDetailResponse xmlns:contrib=\"http://sat.gob.mx/contrib/schemas\">" +
                "   <contrib:contribuyente>" +
                "      <contrib:id>" + controlServicio.getIdControlServicio() + "</contrib:id>" +
                "      <contrib:nombre>SAT</contrib:nombre>" +
                "      <contrib:rfc>SAT960301</contrib:rfc>" +
                "      <contrib:feNac>1996-03-01 00:00:00.0</contrib:feNac>" +
                "   </contrib:contribuyente>" +
                "</contrib:contribuyenteDetailResponse>"
            );

        // Envio de peticion y validacion de respuesta XML.
        this.mockServer.expect(payload(requestPayload)).andRespond(withPayload(responsePayload));

        String resulWS = this.controlServicioServiceClient.invokeControlServicioDetailServiceAndGetASuccessResponse(
                controlServicio.getIdControlServicio().toString());
        LOG.debug("### resulWS=[{}]", resulWS);
        Assert.assertThat(resulWS, containsString("SAT960301"));
        this.mockServer.verify();

        LOG.debug("### << debeDevolverControlServicioCuandoControlServicioDetailRequest()...");
    }

}
