/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.adapter.ws;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

import javax.xml.transform.Source;

import com.creditoparati.avaluos.ws.domain.Cliente;
import com.creditoparati.avaluos.ws.domain.ControlServicio;
import com.creditoparati.avaluos.ws.domain.Controlador;
import com.creditoparati.avaluos.ws.domain.Domicilio;
import com.creditoparati.avaluos.ws.domain.Perito;
import com.creditoparati.avaluos.ws.domain.Servicio;
import com.creditoparati.avaluos.ws.api.ControlServicioService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.xml.transform.StringSource;

import static org.springframework.ws.test.server.RequestCreators.withPayload;
import static org.springframework.ws.test.server.ResponseMatchers.payload;
import static org.springframework.ws.test.server.ResponseMatchers.xpath;

/**
 * Prueba Mock de Integracion sobre el Endpoint de contribuyentes.
 * @author Edgar Deloera.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "file:src/main/webapp/WEB-INF/spring-ws-servlet.xml",
        "classpath*:/META-INF/spring/appCtx-*.xml"
})
public class ControlServicioServiceEndpointIntegrationTest {

    /***
     * Log de clase.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ControlServicioServiceEndpointIntegrationTest.class);

    /**
     * Application context para la prueba.
     */
    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Inyeccion del servicio de negocio del contribuyente, auxiliar en la prueba.
     */
    @Autowired
    private ControlServicioService controlServicioService;

    /**
     * Mock del cliente de WS.
     */
    private MockWebServiceClient mockWSClient;

    /**
     * Un mapeo de name spaces para realizar validaciones con XPATH.
     */
    HashMap<String, String> nameSpaces = new HashMap<String, String>();

    /**
     * Metodo que se ejecuta al inicio del test.
     * Inicializa el mock y asigna un valor al mapa de name spaces.
     */
    @Before
    public void createContribuyenteServiceClient() {
        this.mockWSClient = MockWebServiceClient.createClient(applicationContext);
        nameSpaces.put("sch", "http://www.creditoparati.com.mx/hr/schemas");
    }

    /**
     * Test de la insercion de un contribuyente.
     * @throws Exception
     */
    @Test
    public void debeInsertarControlServicioCuandoControlServicioRequest() throws Exception {
        LOG.debug("### >> debeInsertarControlServicioCuandoControlDeServicio()...");

        // Armado del XML del request.
        Source requestPayload = new StringSource(
        		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>                 " +
        		"    <sch:ControlServicioRequest                          " +
        	    "       xmlns:sch=\"http://www.creditoparati.com.mx/hr/schemas\" >" +
				"        <sch:id_control_servicio></sch:id_control_servicio>" +
				"        <sch:fecha_creacion></sch:fecha_creacion>          " +
				"        <sch:cliente>                                      " +
				"            <sch:tipo>F</sch:tipo>                         " +
				"            <sch:rfc>AAAA000000XXX</sch:rfc>               " +
				"            <sch:nombre>Nombre Cliente</sch:nombre>        " +
				"            <sch:email>correo@mail.com</sch:email>         " +
				"            <sch:cuenta>0000</sch:cuenta>                  " +
				"        </sch:cliente>                                     " +
				"        <sch:domicilio>                                    " +
				"            <sch:calle>calle</sch:calle>                   " +
				"            <sch:colonia>colonia</sch:colonia>             " +
				"            <sch:municipio>014</sch:municipio>             " +
				"            <sch:estado>09</sch:estado>                    " +
				"            <sch:cp>00000</sch:cp>                         " +
				"            <sch:num_exterior>701</sch:num_exterior>       " +
				"            <sch:num_interior>304A</sch:num_interior>      " +
				"        </sch:domicilio>                                   " +
				"        <sch:servicio>                                     " +
				"            <sch:tipo>MA</sch:tipo>                        " +
				"            <sch:remesa>1234567</sch:remesa>                      " +
				"            <sch:servicio>1234567</sch:servicio>                  " +
				"            <sch:fec_generacion>01/01/2013</sch:fec_generacion>   " +
				"            <sch:subtotal>0.00</sch:subtotal>                     " +
				"            <sch:iva>0.00</sch:iva>                               " +
				"            <sch:total>0.00</sch:total>                           " +
				"            <sch:referencia>1234567</sch:referencia>              " +
				"            <sch:sucursal>0000</sch:sucursal>                     " +
				"            <sch:estatus>1</sch:estatus>                          " +
				"        </sch:servicio>                                           " +
				"        <sch:perito>                                              " +
				"            <sch:rfc>AAAA00000XXX</sch:rfc>                       " +
				"            <sch:subtotal>0.00</sch:subtotal>                     " +
				"            <sch:iva>0.00</sch:iva>                               " +
				"            <sch:total>0.00</sch:total>                           " +
				"        </sch:perito>                                             " +
				"        <sch:controlador>                                         " +
				"            <sch:rfc>AAAA000000XXX</sch:rfc>                      " +
				"            <sch:subtotal>0.00</sch:subtotal>                     " +
				"            <sch:iva>0.00</sch:iva>                               " +
				"            <sch:total>0.00</sch:total>                           " +
				"       </sch:controlador>                                         " +
				"   </sch:ControlServicioRequest>                                  "     		
        );

        
        // Declaracion del response esperado.
        Source responsePayload = new StringSource(
        		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>                 " +
        		"    <sch:ControlServicioResponse                          " +
        	    "       xmlns:sch=\"http://www.creditoparati.com.mx/hr/schemas\" >" +
				"        <sch:id_control_servicio></sch:id_control_servicio>" +
				"        <sch:fecha_creacion></sch:fecha_creacion>          " +
				"        <sch:servicio>                                     " +
				"            <sch:tipo></sch:tipo>                          " +
				"            <sch:remesa></sch:remesa>                      " +
				"            <sch:servicio></sch:servicio>                  " +
				"            <sch:fec_generacion></sch:fec_generacion>      " +
				"            <sch:subtotal></sch:subtotal>                  " +
				"            <sch:iva></sch:iva>                            " +
				"            <sch:total></sch:total>                        " +
				"            <sch:referencia></sch:referencia>              " +
				"            <sch:sucursal></sch:sucursal>                  " +
				"            <sch:estatus></sch:estatus>                    " +
				"        </sch:servicio>                                    " +
                "    </sch:ControlServicioResponse>"
            );
        
        
        // Envio de peticion y validacion de respuesta usando XPATH.
        this.mockWSClient.sendRequest(withPayload(requestPayload)).
            andExpect(payload(responsePayload));

        LOG.debug("### << debeInsertarContribuyenteCuandoContribuyenteInsertRequest()...");
    }
}
