/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api;

import com.creditoparati.avaluos.ws.domain.Domicilio;

import java.util.List;

/**
 * Servicio de aplicacion para realizar acciones sobre el objeto de dominio Domicilio.
 *
 * @author Edgar Deloera
 */
public interface DomicilioService {

    /**
     * Obtiene un Domicilio por su id.
     * @param idDomicilio El id del Domicilio a obtener.
     * @return El objeto Domicilio con sus datos o null.
     * @throws DomicilioNotFoundException
     */
    Domicilio findDomicilio(Long idDomicilio) throws DomicilioNotFoundException;

    /**
     * Insertar un Domicilio en la BD.
     * @param Domicilio El Domicilio a ser insertado.
     * @return El Domicilio insertado.
     */
    Domicilio save(Domicilio domicilio);

    List<Domicilio> findAll();

    /**
     * Elimina un Domicilio por su id.
     * @param id El id del Domicilio a eliminar.
     * @throws DomicilioNotFoundException
     */
    void borrar(Long id) throws DomicilioNotFoundException;

    /**
     * Actualiza un Domicilio.
     * @param Domicilio El Domicilio con los datos a actualizar.
     * @return El Domicilio actualizado.
     * @throws DomicilioNotFoundException
     */
    Domicilio actualizar(Domicilio domicilio) throws DomicilioNotFoundException;

}
