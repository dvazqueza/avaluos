/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api;

/**
 * @author Rafael Antonio Guti&eacute;rrez Turullols
 */
public class ControlServicioNotFoundException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2465389974812913055L;

	public ControlServicioNotFoundException() {
    }

    public ControlServicioNotFoundException(String message) {
        super(message);
    }

    public ControlServicioNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ControlServicioNotFoundException(Throwable cause) {
        super(cause);
    }
}
