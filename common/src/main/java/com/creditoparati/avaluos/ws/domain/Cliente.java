/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Objeto de dominio Cliente.
 * @author Edgar Deloera
 */
@Entity
@Table (name = "sm_avaluos_cliente")
public class Cliente {

    /**
     * Id del Contribuyente
     */
    @Id
//    @GeneratedValue(generator = "ClienteSeq")
//    @SequenceGenerator(name = "ClienteSeq", sequenceName = "cliente_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_cliente")
    protected Long idCliente;

    @Column(name = "tipo", nullable = false)
    protected String tipo;   

    @Column(name = "email", nullable = false)
    protected String email;  

    @Column(name = "cuenta", nullable = false)
    protected String cuenta; 

    /**
     * RFC del Contribuyente
     */
    @Column(name = "rfc", nullable = false)
    protected String rfc;

    /**
     * Nombre del Contribuyente
     */
    @Column(name = "nombre", nullable = false)
    protected String nombre;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_control_servicio")
    protected ControlServicio controlServicio;
    
    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long value) {
        this.idCliente = value;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String value) {
        this.rfc = value;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String value) {
        this.nombre = value;
    }

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}

	/**
	 * @param cuenta the cuenta to set
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	/**
	 * @return the controlServicio
	 */
	public ControlServicio getControlServicio() {
		return controlServicio;
	}

	/**
	 * @param controlServicio the controlServicio to set
	 */
	public void setControlServicio(ControlServicio controlServicio) {
		this.controlServicio = controlServicio;
	}

}
