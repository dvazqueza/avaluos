/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.dao;

import com.creditoparati.avaluos.ws.domain.Domicilio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Interfaz del Repository de Contribuyente.
 * @author Edgar Deloera
 */
@Repository
public interface DomicilioRepository extends JpaRepository<Domicilio, Long>, JpaSpecificationExecutor<Domicilio> {

}
