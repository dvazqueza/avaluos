/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api;

import com.creditoparati.avaluos.ws.domain.Pago;

import java.util.List;

/**
 * Servicio de aplicacion para realizar acciones sobre el objeto de dominio Pago.
 *
 * @author Edgar Deloera
 */
public interface PagoService {

    /**
     * Obtiene un Pago por su id.
     * @param idPago El id del Pago a obtener.
     * @return El objeto Pago con sus datos o null.
     * @throws PagoNotFoundException
     */
    Pago findPago(Long idPago) throws PagoNotFoundException;

    /**
     * Insertar un Pago en la BD.
     * @param Pago El Pago a ser insertado.
     * @return El Pago insertado.
     */
    Pago save(Pago pago);

    List<Pago> findAll();

    /**
     * Elimina un Pago por su id.
     * @param id El id del Pago a eliminar.
     * @throws PagoNotFoundException
     */
    void borrar(Long id) throws PagoNotFoundException;

    /**
     * Actualiza un Pago.
     * @param Pago El Pago con los datos a actualizar.
     * @return El Pago actualizado.
     * @throws PagoNotFoundException
     */
    Pago actualizar(Pago pago) throws PagoNotFoundException;

}
