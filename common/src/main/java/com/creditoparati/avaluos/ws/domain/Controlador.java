package com.creditoparati.avaluos.ws.domain;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Objeto de dominio Contribuyente.
 * @author Edgar Deloera
 */
@Entity
@Table (name = "sm_avaluos_controlador")
public class Controlador {

    /**
     * Id del Contribuyente
     */
    @Id
//    @GeneratedValue(generator = "ControladorSeq")
//    @SequenceGenerator(name = "ControladorSeq", sequenceName = "controlador_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_controlador")
    protected Long idControlador;

    /**
     * RFC del Contribuyente
     */
    @Column(name = "rfc", nullable = false)
    protected String rfc;

    
    @Column(name = "subtotal", nullable = false)
    protected BigDecimal subtotal;
    
    @Column(name = "iva", nullable = false)
    protected BigDecimal iva;
    
    @Column(name = "total", nullable = false)
    protected BigDecimal total;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_control_servicio")
    protected ControlServicio controlServicio;
    
    public Long getIdControlador() {
        return idControlador;
    }

    public void setIdControlador(Long value) {
        this.idControlador = value;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String value) {
        this.rfc = value;
    }

	/**
	 * @return the subtotal
	 */
	public BigDecimal getSubtotal() {
		return subtotal;
	}

	/**
	 * @param subtotal the subtotal to set
	 */
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	/**
	 * @return the iva
	 */
	public BigDecimal getIva() {
		return iva;
	}

	/**
	 * @param iva the iva to set
	 */
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}

    public void setSubtotalString(String value) {
        try {
        	this.setSubtotal(new BigDecimal(value));
        } catch (ArithmeticException ae) {
            this.setSubtotal(new BigDecimal(0));
            ae.printStackTrace();
        } catch (NumberFormatException nfe) {
            this.setSubtotal(new BigDecimal(0));
            nfe.printStackTrace();
        }
    }
    
    public void setIvaString(String value) {
        try {
        	this.setIva(new BigDecimal(value));
        } catch (ArithmeticException ae) {
            this.setIva(new BigDecimal(0));
            ae.printStackTrace();
        } catch (NumberFormatException nfe) {
            this.setIva(new BigDecimal(0));
            nfe.printStackTrace();
        }
    }
    
    public void setTotalString(String value) {
        try {
        	this.setTotal(new BigDecimal(value));
        } catch (ArithmeticException ae) {
            this.setTotal(new BigDecimal(0));
            ae.printStackTrace();
        } catch (NumberFormatException nfe) {
            this.setTotal(new BigDecimal(0));
            nfe.printStackTrace();
        }
    }

	/**
	 * @return the controlServicio
	 */
	public ControlServicio getControlServicio() {
		return controlServicio;
	}

	/**
	 * @param controlServicio the controlServicio to set
	 */
	public void setControlServicio(ControlServicio controlServicio) {
		this.controlServicio = controlServicio;
	}

}
