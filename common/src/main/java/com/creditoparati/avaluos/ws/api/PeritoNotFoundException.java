/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api;

/**
 * @author Rafael Antonio Guti&eacute;rrez Turullols
 */
public class PeritoNotFoundException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1038193604958133578L;

	public PeritoNotFoundException() {
    }

    public PeritoNotFoundException(String message) {
        super(message);
    }

    public PeritoNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PeritoNotFoundException(Throwable cause) {
        super(cause);
    }
}
