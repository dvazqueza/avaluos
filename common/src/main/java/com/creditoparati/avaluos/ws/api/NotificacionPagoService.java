/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api;

import com.creditoparati.avaluos.ws.domain.NotificacionPago;

import java.util.List;

/**
 * Servicio de aplicacion para realizar acciones sobre el objeto de dominio Notificacion.
 *
 * @author Edgar Deloera
 */
public interface NotificacionPagoService {

    /**
     * Obtiene un Notificacion por su id.
     * @param idNotificacion El id del Notificacion a obtener.
     * @return El objeto Notificacion con sus datos o null.
     * @throws NotificacionNotFoundException
     */
	NotificacionPago findNotificacionPago(Long idNotificacionPago) throws NotificacionPagoNotFoundException;

    /**
     * Insertar un Notificacion en la BD.
     * @param Notificacion El Notificacion a ser insertado.
     * @return El Notificacion insertado.
     */
    NotificacionPago save(NotificacionPago notificacion);

    List<NotificacionPago> findAll();

    /**
     * Elimina un Notificacion por su id.
     * @param id El id del Notificacion a eliminar.
     * @throws NotificacionNotFoundException
     */
    void borrar(Long id) throws NotificacionPagoNotFoundException;

    /**
     * Actualiza un Notificacion.
     * @param Notificacion El Notificacion con los datos a actualizar.
     * @return El Notificacion actualizado.
     * @throws NotificacionNotFoundException
     */
    NotificacionPago actualizar(NotificacionPago notificacion) throws NotificacionPagoNotFoundException;

}
