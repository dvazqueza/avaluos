/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Objeto de dominio Contribuyente.
 * @author Edgar Deloera
 */
@Entity
@Table (name = "sm_avaluos_domicilio")
public class Domicilio {

    /**
     * Id del Contribuyente
     */
    @Id
//    @GeneratedValue(generator = "DomicilioSeq")
//    @SequenceGenerator(name = "DomicilioSeq", sequenceName = "domicilio_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_domicilio")
    protected Long idDomicilio;

    
    @Column(name = "calle", nullable = false)
    protected String calle;      
    
    @Column(name = "num_exterior", nullable = false)
    protected String numExterior;
    
    @Column(name = "num_interior", nullable = true)
    protected String numInterior;
    
    @Column(name = "colonia", nullable = false)
    protected String colonia;
    
    @Column(name = "municipio", nullable = false)
    protected String municipio;
    
    @Column(name = "estado", nullable = false)
    protected String estado;
    
    @Column(name = "cp", nullable = false)
    protected String cp;
                
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_control_servicio")
    protected ControlServicio controlServicio;

    public Long getIdDomicilio() {
        return idDomicilio;
    }

    public void setIdDomicilio(Long value) {
        this.idDomicilio = value;
    }

	/**
	 * @return the calle
	 */
	public String getCalle() {
		return calle;
	}

	/**
	 * @param calle the calle to set
	 */
	public void setCalle(String calle) {
		this.calle = calle;
	}

	/**
	 * @return the num_exterior
	 */
	public String getNumExterior() {
		return numExterior;
	}

	/**
	 * @param num_exterior the num_exterior to set
	 */
	public void setNumExterior(String numExterior) {
		this.numExterior = numExterior;
	}

	/**
	 * @return the num_interior
	 */
	public String getNumInterior() {
		return numInterior;
	}

	/**
	 * @param num_interior the num_interior to set
	 */
	public void setNumInterior(String numInterior) {
		this.numInterior = numInterior;
	}

	/**
	 * @return the colonia
	 */
	public String getColonia() {
		return colonia;
	}

	/**
	 * @param colonia the colonia to set
	 */
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	/**
	 * @return the municipio
	 */
	public String getMunicipio() {
		return municipio;
	}

	/**
	 * @param municipio the municipio to set
	 */
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the cp
	 */
	public String getCp() {
		return cp;
	}

	/**
	 * @param cp the cp to set
	 */
	public void setCp(String cp) {
		this.cp = cp;
	}

	/**
	 * @return the controlServicio
	 */
	public ControlServicio getControlServicio() {
		return controlServicio;
	}

	/**
	 * @param controlServicio the controlServicio to set
	 */
	public void setControlServicio(ControlServicio controlServicio) {
		this.controlServicio = controlServicio;
	}

 
}
