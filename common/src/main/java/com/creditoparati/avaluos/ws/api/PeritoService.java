/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api;

import com.creditoparati.avaluos.ws.domain.Perito;

import java.util.List;

/**
 * Servicio de aplicacion para realizar acciones sobre el objeto de dominio Perito.
 *
 * @author Edgar Deloera
 */
public interface PeritoService {

    /**
     * Obtiene un Perito por su id.
     * @param idPerito El id del Perito a obtener.
     * @return El objeto Perito con sus datos o null.
     * @throws PeritoNotFoundException
     */
    Perito findPerito(Long idPerito) throws PeritoNotFoundException;

    /**
     * Insertar un Perito en la BD.
     * @param Perito El Perito a ser insertado.
     * @return El Perito insertado.
     */
    Perito save(Perito perito);

    List<Perito> findAll();

    /**
     * Elimina un Perito por su id.
     * @param id El id del Perito a eliminar.
     * @throws PeritoNotFoundException
     */
    void borrar(Long id) throws PeritoNotFoundException;

    /**
     * Actualiza un Perito.
     * @param Perito El Perito con los datos a actualizar.
     * @return El Perito actualizado.
     * @throws PeritoNotFoundException
     */
    Perito actualizar(Perito perito) throws PeritoNotFoundException;

}
