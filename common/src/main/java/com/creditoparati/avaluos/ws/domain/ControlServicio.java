/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Objeto de dominio Contribuyente.
 * @author Edgar Deloera
 */
@Entity
@Table (name = "sm_avaluos_control_servicio")
public class ControlServicio {

    /**
     * Id del Contribuyente
     */
    @Id
//    @GeneratedValue(generator = "ControlServicioSeq")
//    @SequenceGenerator(name = "ControlServicioSeq", sequenceName = "control_servicio_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_control_servicio")
    protected Long idControlServicio;

    @Column(name = "fecha_creacion")
    protected Date fechaCreacion;
    
    public Long getIdControlServicio() {
        return idControlServicio;
    }

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getFechaCreacionString() {
		SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");
		return formatoDeFecha.format(fechaCreacion);
	}
	
    public void setIdControlServicio(Long value) {
        this.idControlServicio = value;
    }

    @OneToOne(mappedBy="controlServicio", cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	protected Cliente cliente = new Cliente();
    
    public Cliente getCliente() {
		return cliente;
	} 

	public void setCliente(Cliente cliente) {
		cliente.setControlServicio(this);
		this.cliente = cliente;
	}

    @OneToOne(mappedBy="controlServicio", cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	protected Domicilio domicilio = new Domicilio();
    
    public Domicilio getDomicilio() {
		return domicilio;
	} 

	public void setDomicilio(Domicilio domicilio) {
		domicilio.setControlServicio(this);
		this.domicilio = domicilio;
	}

	
    @OneToOne(mappedBy="controlServicio", cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	protected Servicio servicio = new Servicio();
    
    public Servicio getServicio() {
		return servicio;
	} 

	public void setServicio(Servicio servicio) {
		servicio.setControlServicio(this);
		this.servicio = servicio;
	}

    @OneToOne(mappedBy="controlServicio", cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	protected Perito perito = new Perito();
    
    public Perito getPerito() {
		return perito;
	} 

	public void setPerito(Perito perito) {
		perito.setControlServicio(this);
		this.perito = perito;
	}

    @OneToOne(mappedBy="controlServicio", cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	protected Controlador controlador = new Controlador();
    
    public Controlador getControlador() {
		return controlador;
	} 

	public void setControlador(Controlador controlador) {
		controlador.setControlServicio(this);
		this.controlador = controlador;
	}
}
