/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Objeto de dominio Contribuyente.
 * @author Edgar Deloera
 */
@Entity
@Table (name = "sm_avaluos_notificacion_pago")
public class NotificacionPago {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
//    @GeneratedValue(generator = "NotificacionPagoSeq")
//    @SequenceGenerator(name = "NotificacionPagoSeq", sequenceName = "notificacion_pago_seq", allocationSize = 1)
    @Column(name = "id_notificacion_pago")
    protected Long idNotificacionPago;

    @Column(name = "fecha_creacion")
    protected Date fechaCreacion;
    
    @OneToMany(mappedBy="idPago", cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	protected Set<Pago> pagos = new HashSet<Pago>();
   
	public Long getIdNotificacionPago() {
		return idNotificacionPago;
	}

	public void setIdNotificacionPago(Long idNotificacionPago) {
		this.idNotificacionPago = idNotificacionPago;
	}

    public Set<Pago> getPagos() {
		return pagos;
	} 

	public void setPagos(Set<Pago> pagos) {
		this.pagos = pagos;
	}

	public void addPago(Pago pago) {
		pago.setNotificacionPago(this);
		pagos.add(pago);
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
}
