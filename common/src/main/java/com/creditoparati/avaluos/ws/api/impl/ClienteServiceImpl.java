/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api.impl;

import java.util.List;

import com.creditoparati.avaluos.ws.api.ClienteNotFoundException;
import com.creditoparati.avaluos.ws.api.ClienteService;
import com.creditoparati.avaluos.ws.dao.ClienteRepository;
import com.creditoparati.avaluos.ws.domain.Cliente;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementacion del servicio de aplicacion para realizar acciones sobre el objeto de dominio Cliente.
 * @author Edgar Deloera
 */
@Service("clienteApiService")
public class ClienteServiceImpl implements ClienteService {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ClienteServiceImpl.class);

    @Autowired
    private ClienteRepository clienteRepository;
    
    /**
     * @see mx.gob.sat.siat.poc.ws.api.ClienteService#findCliente(Long)
     */
    @Override
    public Cliente findCliente(Long idCliente) throws ClienteNotFoundException{
        LOG.debug("### >> findCliente(idCliente=[{}]) ###", idCliente);

        Cliente cont = this.clienteRepository.findOne(idCliente);
        if (cont != null && cont.getIdCliente() != null) {
            LOG.debug("### << findCliente() ###");
            return cont;
        } else {
            throw new ClienteNotFoundException("Cliente con id=[" +
                    idCliente.toString() + "] no se encuentra!");
        }
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.ClienteService#save(Cliente)
     */
    @Override
    public List<Cliente> findAll() {
        LOG.debug("### >> findAll() ###");

        List<Cliente> cs = clienteRepository.findAll();

        LOG.debug("### << findAll(): [{}] ###", cs.size());
        return cs;
    }

    @Override
    public Cliente save(Cliente cliente) {
        LOG.debug("### >> save() ###");

        LOG.debug("### >> save() ###");
        Cliente cont = this.clienteRepository.saveAndFlush(cliente);
        LOG.debug("### << save(): [{}] ###", cont.getIdCliente());
        return cont;
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.ClienteService#borrar(Long)
     */
    @Override
    public void borrar(Long id) throws ClienteNotFoundException {
        LOG.debug("### >> borrar() ###");
        Cliente c = this.clienteRepository.findOne(id);
        if (c == null) {
            throw new ClienteNotFoundException();
        }

        this.clienteRepository.delete(id);
        LOG.debug("### <<s borrar() ###");
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.ClienteService#actualizar(Cliente)
     */
    @Override
    public Cliente actualizar(Cliente cliente) throws ClienteNotFoundException {
        LOG.info("######### ClienteApiServiceImpl.actualizar()...");
        
        Cliente cont = this.clienteRepository.saveAndFlush(cliente);
        if (cont != null && cont.getIdCliente() != null) {
            LOG.info("### << actualizar() ###");
            return cont;
        } else {
            throw new ClienteNotFoundException("Cliente con id=[" +
                    cliente.getIdCliente().toString() + "] no se encuentra!");
        }
    }

}
