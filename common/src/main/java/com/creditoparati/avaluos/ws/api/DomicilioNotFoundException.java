/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api;

/**
 * @author Rafael Antonio Guti&eacute;rrez Turullols
 */
public class DomicilioNotFoundException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4775420977112569012L;

	public DomicilioNotFoundException() {
    }

    public DomicilioNotFoundException(String message) {
        super(message);
    }

    public DomicilioNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public DomicilioNotFoundException(Throwable cause) {
        super(cause);
    }
}
