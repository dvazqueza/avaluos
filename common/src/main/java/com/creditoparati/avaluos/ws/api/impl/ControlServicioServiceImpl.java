/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api.impl;

import java.util.List;

import com.creditoparati.avaluos.ws.api.ControlServicioNotFoundException;
import com.creditoparati.avaluos.ws.api.ControlServicioService;
import com.creditoparati.avaluos.ws.dao.ControlServicioRepository;
import com.creditoparati.avaluos.ws.domain.ControlServicio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementacion del servicio de aplicacion para realizar acciones sobre el objeto de dominio ControlServicio.
 * @author Edgar Deloera
 */
@Service("controlServicioApiService")
public class ControlServicioServiceImpl implements ControlServicioService {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ControlServicioServiceImpl.class);

    @Autowired
    private ControlServicioRepository controlServicioRepository;
    
    /**
     * @see mx.gob.sat.siat.poc.ws.api.ControlServicioService#findControlServicio(Long)
     */
    @Override
    public ControlServicio findControlServicio(Long idControlServicio) throws ControlServicioNotFoundException{
        LOG.debug("### >> findControlServicio(idControlServicio=[{}]) ###", idControlServicio);

        ControlServicio cont = this.controlServicioRepository.findOne(idControlServicio);
        if (cont != null && cont.getIdControlServicio() != null) {
            LOG.debug("### << findControlServicio() ###");
            return cont;
        } else {
            throw new ControlServicioNotFoundException("ControlServicio con id=[" +
                    idControlServicio.toString() + "] no se encuentra!");
        }
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.ControlServicioService#save(ControlServicio)
     */
    @Override
    public List<ControlServicio> findAll() {
        LOG.debug("### >> findAll() ###");

        List<ControlServicio> cs = controlServicioRepository.findAll();

        LOG.debug("### << findAll(): [{}] ###", cs.size());
        return cs;
    }

    @Override
    public ControlServicio save(ControlServicio controlServicio) {
        LOG.debug("### >> save() ###");
        ControlServicio cont = this.controlServicioRepository.saveAndFlush(controlServicio);
        LOG.debug("### << save(): [{}] ###", cont.getIdControlServicio());
        return cont;
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.ControlServicioService#borrar(Long)
     */
    @Override
    public void borrar(Long id) throws ControlServicioNotFoundException {
        LOG.debug("### >> borrar() ###");
        ControlServicio c = this.controlServicioRepository.findOne(id);
        if (c == null) {
            throw new ControlServicioNotFoundException();
        }

        this.controlServicioRepository.delete(id);
        LOG.debug("### <<s borrar() ###");
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.ControlServicioService#actualizar(ControlServicio)
     */
    @Override
    public ControlServicio actualizar(ControlServicio controlServicio) throws ControlServicioNotFoundException {
        LOG.info("######### ControlServicioApiServiceImpl.actualizar()...");
        
        ControlServicio cont = this.controlServicioRepository.saveAndFlush(controlServicio);
        if (cont != null && cont.getIdControlServicio() != null) {
            LOG.info("### << actualizar() ###");
            return cont;
        } else {
            throw new ControlServicioNotFoundException("ControlServicio con id=[" +
                    controlServicio.getIdControlServicio().toString() + "] no se encuentra!");
        }
    }

}
