package com.creditoparati.avaluos.ws.domain;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Objeto de dominio Contribuyente.
 * @author Edgar Deloera
 */
@Entity
@Table (name = "sm_avaluos_pago")
public class Pago {

    /**
     * Id del Contribuyente
     */
    @Id
    // @GeneratedValue(generator = "PagoSeq")
    // @SequenceGenerator(name = "PagoSeq", sequenceName = "pago_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_pago")
    protected Long idPago;

    @Column(name = "subtotal", nullable = false)
    protected BigDecimal subtotal;
    
    @Column(name = "iva", nullable = false)
    protected BigDecimal iva;
    
    @Column(name = "total", nullable = false)
    protected BigDecimal total;
    
    @Column(name = "remesa", nullable = false)
    protected String remesa;
    
    @Column(name = "servicio", nullable = false)
    protected String servicio;
    
    @Column(name = "factura", nullable = false)
    protected String factura;
    
    @Column(name = "referencia", nullable = false)
    protected String referencia;
    
    @Column(name = "sucursal", nullable = false)
    protected String sucursal;
    
    @Column(name = "estatus", nullable = false)
    protected String estatus;
    /**
     * Nombre del Contribuyente
     */
    @Column(name = "tipo", nullable = false)
    protected String tipo;

    /**
     * Fecha de Nacimiento del Contribuyente
     */
    @Column(name = "fecha", nullable = false)
    protected Date fecha;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_notificacion_pago")
    protected NotificacionPago notificacionPago;
    
    public Long getIdPago() {
        return idPago;
    }

    public void setIdPago(Long value) {
        this.idPago = value;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String value) {
        this.tipo = value;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date value) {
        this.fecha = value;
    }

    public void setFechaString(String value) {
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");
        try {
            this.setFecha(formatoDeFecha.parse(value));
        } catch (ParseException pe) {
            this.setFecha(new Date());
            pe.printStackTrace();
        }
    }

	/**
	 * @return the subtotal
	 */
	public BigDecimal getSubtotal() {
		return subtotal;
	}

	/**
	 * @param subtotal the subtotal to set
	 */
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

    public void setSubtotalString(String value) {
        try {
        	this.setSubtotal(new BigDecimal(value));
        } catch (ArithmeticException ae) {
            this.setSubtotal(new BigDecimal(0));
            ae.printStackTrace();
        } catch (NumberFormatException nfe) {
            this.setSubtotal(new BigDecimal(0));
            nfe.printStackTrace();
        }
    }
    
    public void setIvaString(String value) {
        try {
        	this.setIva(new BigDecimal(value));
        } catch (ArithmeticException ae) {
            this.setIva(new BigDecimal(0));
            ae.printStackTrace();
        } catch (NumberFormatException nfe) {
            this.setIva(new BigDecimal(0));
            nfe.printStackTrace();
        }
    }
    
    public void setTotalString(String value) {
        try {
        	this.setTotal(new BigDecimal(value));
        } catch (ArithmeticException ae) {
            this.setTotal(new BigDecimal(0));
            ae.printStackTrace();
        } catch (NumberFormatException nfe) {
            this.setTotal(new BigDecimal(0));
            nfe.printStackTrace();
        }
    }
    
	/**
	 * @return the iva
	 */
	public BigDecimal getIva() {
		return iva;
	}

	/**
	 * @param iva the iva to set
	 */
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	/**
	 * @return the remesa
	 */
	public String getRemesa() {
		return remesa;
	}

	/**
	 * @param remesa the remesa to set
	 */
	public void setRemesa(String remesa) {
		this.remesa = remesa;
	}

	/**
	 * @return the servicio
	 */
	public String getServicio() {
		return servicio;
	}

	/**
	 * @param servicio the servicio to set
	 */
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	/**
	 * @return the factura
	 */
	public String getFactura() {
		return factura;
	}

	/**
	 * @param factura the factura to set
	 */
	public void setFactura(String factura) {
		this.factura = factura;
	}

	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	/**
	 * @return the sucursal
	 */
	public String getSucursal() {
		return sucursal;
	}

	/**
	 * @param sucursal the sucursal to set
	 */
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public NotificacionPago getNotificacionPago() {
		return notificacionPago;
	}

	public void setNotificacionPago(NotificacionPago notificacionPago) {
		this.notificacionPago = notificacionPago;
	}
}
