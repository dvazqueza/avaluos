/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api;

import com.creditoparati.avaluos.ws.domain.Controlador;

import java.util.List;

/**
 * Servicio de aplicacion para realizar acciones sobre el objeto de dominio Controlador.
 *
 * @author Edgar Deloera
 */
public interface ControladorService {

    /**
     * Obtiene un Controlador por su id.
     * @param idControlador El id del Controlador a obtener.
     * @return El objeto Controlador con sus datos o null.
     * @throws ControladorNotFoundException
     */
    Controlador findControlador(Long idControlador) throws ControladorNotFoundException;

    /**
     * Insertar un Controlador en la BD.
     * @param Controlador El Controlador a ser insertado.
     * @return El Controlador insertado.
     */
    Controlador save(Controlador controlador);

    List<Controlador> findAll();

    /**
     * Elimina un Controlador por su id.
     * @param id El id del Controlador a eliminar.
     * @throws ControladorNotFoundException
     */
    void borrar(Long id) throws ControladorNotFoundException;

    /**
     * Actualiza un Controlador.
     * @param Controlador El Controlador con los datos a actualizar.
     * @return El Controlador actualizado.
     * @throws ControladorNotFoundException
     */
    Controlador actualizar(Controlador controlador) throws ControladorNotFoundException;

}
