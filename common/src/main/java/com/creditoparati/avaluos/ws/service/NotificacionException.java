package com.creditoparati.avaluos.ws.service;

public class NotificacionException extends Exception {

    private static final long serialVersionUID = 1L;

    private String message;

    private Object sender;

    /**
     * Constructor de clase.
     * @param sender Objeto que env&iacute;a la excepci&oacute;n.
     * @param message Mensaje de la excepci&oacute;n.
     */
    public NotificacionException(Object sender, String message) {
        super(message);
        this.message = message;
        this.sender = sender;
    }

    /**
     * @see java.lang.Throwable#getMessage().
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Obtiene el sender.
     * @return Regresa el valor de sender.
     */
    public Object getSender() {
        return sender;
    }

}
