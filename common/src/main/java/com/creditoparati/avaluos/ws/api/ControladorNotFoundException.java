/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api;

/**
 * @author Rafael Antonio Guti&eacute;rrez Turullols
 */
public class ControladorNotFoundException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 2634011399255516732L;

	public ControladorNotFoundException() {
    }

    public ControladorNotFoundException(String message) {
        super(message);
    }

    public ControladorNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ControladorNotFoundException(Throwable cause) {
        super(cause);
    }
}
