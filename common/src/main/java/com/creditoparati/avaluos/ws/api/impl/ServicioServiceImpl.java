/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api.impl;

import java.util.List;

import com.creditoparati.avaluos.ws.api.ServicioNotFoundException;
import com.creditoparati.avaluos.ws.api.ServicioService;
import com.creditoparati.avaluos.ws.dao.ServicioRepository;
import com.creditoparati.avaluos.ws.domain.Servicio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementacion del servicio de aplicacion para realizar acciones sobre el objeto de dominio Servicio.
 * @author Edgar Deloera
 */
@Service("servicioApiService")
public class ServicioServiceImpl implements ServicioService {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ServicioServiceImpl.class);

    @Autowired
    private ServicioRepository servicioRepository;
    
    /**
     * @see mx.gob.sat.siat.poc.ws.api.ServicioService#findServicio(Long)
     */
    @Override
    public Servicio findServicio(Long idServicio) throws ServicioNotFoundException{
        LOG.debug("### >> findServicio(idServicio=[{}]) ###", idServicio);

        Servicio servicio = this.servicioRepository.findOne(idServicio);
        if (servicio != null && servicio.getIdServicio() != null) {
            LOG.debug("### << findServicio() ###");
            return servicio;
        } else {
            throw new ServicioNotFoundException("Servicio con id=[" +
                    idServicio.toString() + "] no se encuentra!");
        }
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.ServicioService#save(Servicio)
     */
    @Override
    public List<Servicio> findAll() {
        LOG.debug("### >> findAll() ###");

        List<Servicio> cs = servicioRepository.findAll();

        LOG.debug("### << findAll(): [{}] ###", cs.size());
        return cs;
    }

    @Override
    public Servicio save(Servicio servicio) {
        LOG.debug("### >> save() ###");

        LOG.debug("### >> save() ###");
        Servicio cont = this.servicioRepository.saveAndFlush(servicio);
        LOG.debug("### << save(): [{}] ###", cont.getIdServicio());
        return cont;
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.ServicioService#borrar(Long)
     */
    @Override
    public void borrar(Long id) throws ServicioNotFoundException {
        LOG.debug("### >> borrar() ###");
        Servicio c = this.servicioRepository.findOne(id);
        if (c == null) {
            throw new ServicioNotFoundException();
        }

        this.servicioRepository.delete(id);
        LOG.debug("### <<s borrar() ###");
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.ServicioService#actualizar(Servicio)
     */
    @Override
    public Servicio actualizar(Servicio servicio) throws ServicioNotFoundException {
        LOG.info("######### ServicioApiServiceImpl.actualizar()...");
        
        Servicio cont = this.servicioRepository.saveAndFlush(servicio);
        if (cont != null && cont.getIdServicio() != null) {
            LOG.info("### << actualizar() ###");
            return cont;
        } else {
            throw new ServicioNotFoundException("Servicio con id=[" +
                    servicio.getIdServicio().toString() + "] no se encuentra!");
        }
    }

}
