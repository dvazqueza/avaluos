/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api.impl;

import java.util.List;

import com.creditoparati.avaluos.ws.api.NotificacionPagoNotFoundException;
import com.creditoparati.avaluos.ws.api.NotificacionPagoService;
import com.creditoparati.avaluos.ws.dao.NotificacionPagoRepository;
import com.creditoparati.avaluos.ws.domain.NotificacionPago;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementacion del servicio de aplicacion para realizar acciones sobre el objeto de dominio NotificacionPago.
 * @author Edgar Deloera
 */
@Service("notificacionPagoApiService")
public class NotificacionPagoServiceImpl implements NotificacionPagoService {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(NotificacionPagoServiceImpl.class);

    @Autowired
    private NotificacionPagoRepository notificacionPagoRepository;
    
    /**
     * @see mx.gob.sat.siat.poc.ws.api.NotificacionPagoService#findNotificacionPago(Long)
     */
    @Override
    public NotificacionPago findNotificacionPago(Long idNotificacionPago) throws NotificacionPagoNotFoundException{
        LOG.debug("### >> findNotificacionPago(idNotificacionPago=[{}]) ###", idNotificacionPago);

        NotificacionPago cont = this.notificacionPagoRepository.findOne(idNotificacionPago);
        if (cont != null && cont.getIdNotificacionPago() != null) {
            LOG.debug("### << findNotificacionPago() ###");
            return cont;
        } else {
            throw new NotificacionPagoNotFoundException("NotificacionPago con id=[" +
                    idNotificacionPago.toString() + "] no se encuentra!");
        }
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.NotificacionPagoService#save(NotificacionPago)
     */
    @Override
    public List<NotificacionPago> findAll() {
        LOG.debug("### >> findAll() ###");

        List<NotificacionPago> cs = notificacionPagoRepository.findAll();

        LOG.debug("### << findAll(): [{}] ###", cs.size());
        return cs;
    }

    @Override
    public NotificacionPago save(NotificacionPago notificacionPago) {
        LOG.debug("### >> save() ###");

        LOG.debug("### >> save() ###");
        NotificacionPago cont = this.notificacionPagoRepository.saveAndFlush(notificacionPago);
        LOG.debug("### << save(): [{}] ###", cont.getIdNotificacionPago());
        return cont;
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.NotificacionPagoService#borrar(Long)
     */
    @Override
    public void borrar(Long id) throws NotificacionPagoNotFoundException {
        LOG.debug("### >> borrar() ###");
        NotificacionPago c = this.notificacionPagoRepository.findOne(id);
        if (c == null) {
            throw new NotificacionPagoNotFoundException();
        }

        this.notificacionPagoRepository.delete(id);
        LOG.debug("### <<s borrar() ###");
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.NotificacionPagoService#actualizar(NotificacionPago)
     */
    @Override
    public NotificacionPago actualizar(NotificacionPago NotificacionPago) throws NotificacionPagoNotFoundException {
        LOG.info("######### NotificacionPagoApiServiceImpl.actualizar()...");
        
        NotificacionPago cont = this.notificacionPagoRepository.saveAndFlush(NotificacionPago);
        if (cont != null && cont.getIdNotificacionPago() != null) {
            LOG.info("### << actualizar() ###");
            return cont;
        } else {
            throw new NotificacionPagoNotFoundException("NotificacionPago con id=[" +
                    NotificacionPago.getIdNotificacionPago().toString() + "] no se encuentra!");
        }
    }

}
