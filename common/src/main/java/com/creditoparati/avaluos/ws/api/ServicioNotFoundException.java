/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api;

/**
 * @author Rafael Antonio Guti&eacute;rrez Turullols
 */
public class ServicioNotFoundException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2258195724562142711L;

	public ServicioNotFoundException() {
    }

    public ServicioNotFoundException(String message) {
        super(message);
    }

    public ServicioNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServicioNotFoundException(Throwable cause) {
        super(cause);
    }
}
