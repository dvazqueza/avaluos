/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api.impl;

import java.util.List;

import com.creditoparati.avaluos.ws.api.DomicilioNotFoundException;
import com.creditoparati.avaluos.ws.api.DomicilioService;
import com.creditoparati.avaluos.ws.dao.DomicilioRepository;
import com.creditoparati.avaluos.ws.domain.Domicilio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementacion del servicio de aplicacion para realizar acciones sobre el objeto de dominio Domicilio.
 * @author Edgar Deloera
 */
@Service("domicilioApiService")
public class DomicilioServiceImpl implements DomicilioService {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DomicilioServiceImpl.class);

    @Autowired
    private DomicilioRepository domicilioRepository;
    
    /**
     * @see mx.gob.sat.siat.poc.ws.api.DomicilioService#findDomicilio(Long)
     */
    @Override
    public Domicilio findDomicilio(Long idDomicilio) throws DomicilioNotFoundException{
        LOG.debug("### >> findDomicilio(idDomicilio=[{}]) ###", idDomicilio);

        Domicilio cont = this.domicilioRepository.findOne(idDomicilio);
        if (cont != null && cont.getIdDomicilio() != null) {
            LOG.debug("### << findDomicilio() ###");
            return cont;
        } else {
            throw new DomicilioNotFoundException("Domicilio con id=[" +
                    idDomicilio.toString() + "] no se encuentra!");
        }
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.DomicilioService#save(Domicilio)
     */
    @Override
    public List<Domicilio> findAll() {
        LOG.debug("### >> findAll() ###");

        List<Domicilio> cs = domicilioRepository.findAll();

        LOG.debug("### << findAll(): [{}] ###", cs.size());
        return cs;
    }

    @Override
    public Domicilio save(Domicilio domicilio) {
        LOG.debug("### >> save() ###");

        LOG.debug("### >> save() ###");
        Domicilio cont = this.domicilioRepository.saveAndFlush(domicilio);
        LOG.debug("### << save(): [{}] ###", cont.getIdDomicilio());
        return cont;
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.DomicilioService#borrar(Long)
     */
    @Override
    public void borrar(Long id) throws DomicilioNotFoundException {
        LOG.debug("### >> borrar() ###");
        Domicilio c = this.domicilioRepository.findOne(id);
        if (c == null) {
            throw new DomicilioNotFoundException();
        }

        this.domicilioRepository.delete(id);
        LOG.debug("### <<s borrar() ###");
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.DomicilioService#actualizar(Domicilio)
     */
    @Override
    public Domicilio actualizar(Domicilio domicilio) throws DomicilioNotFoundException {
        LOG.info("######### DomicilioApiServiceImpl.actualizar()...");
        
        Domicilio cont = this.domicilioRepository.saveAndFlush(domicilio);
        if (cont != null && cont.getIdDomicilio() != null) {
            LOG.info("### << actualizar() ###");
            return cont;
        } else {
            throw new DomicilioNotFoundException("Domicilio con id=[" +
                    domicilio.getIdDomicilio().toString() + "] no se encuentra!");
        }
    }

}
