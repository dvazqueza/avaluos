/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api;

/**
 * @author Rafael Antonio Guti&eacute;rrez Turullols
 */
public class PagoNotFoundException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3846316931652124689L;

	public PagoNotFoundException() {
    }

    public PagoNotFoundException(String message) {
        super(message);
    }

    public PagoNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PagoNotFoundException(Throwable cause) {
        super(cause);
    }
}
