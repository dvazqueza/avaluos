/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api;

import com.creditoparati.avaluos.ws.domain.ControlServicio;

import java.util.List;

/**
 * Servicio de aplicacion para realizar acciones sobre el objeto de dominio ControlServicio.
 *
 * @author Edgar Deloera
 */
public interface ControlServicioService {

    /**
     * Obtiene un ControlServicio por su id.
     * @param idControlServicio El id del ControlServicio a obtener.
     * @return El objeto ControlServicio con sus datos o null.
     * @throws ControlServicioNotFoundException
     */
    ControlServicio findControlServicio(Long idControlServicio) throws ControlServicioNotFoundException;

    /**
     * Insertar un ControlServicio en la BD.
     * @param ControlServicio El ControlServicio a ser insertado.
     * @return El ControlServicio insertado.
     */
    ControlServicio save(ControlServicio controlServicio);

    List<ControlServicio> findAll();

    /**
     * Elimina un ControlServicio por su id.
     * @param id El id del ControlServicio a eliminar.
     * @throws ControlServicioNotFoundException
     */
    void borrar(Long id) throws ControlServicioNotFoundException;

    /**
     * Actualiza un ControlServicio.
     * @param ControlServicio El ControlServicio con los datos a actualizar.
     * @return El ControlServicio actualizado.
     * @throws ControlServicioNotFoundException
     */
    ControlServicio actualizar(ControlServicio controlServicio) throws ControlServicioNotFoundException;

}
