/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api.impl;

import java.util.List;

import com.creditoparati.avaluos.ws.api.PeritoNotFoundException;
import com.creditoparati.avaluos.ws.api.PeritoService;
import com.creditoparati.avaluos.ws.dao.PeritoRepository;
import com.creditoparati.avaluos.ws.domain.Perito;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementacion del servicio de aplicacion para realizar acciones sobre el objeto de dominio Perito.
 * @author Edgar Deloera
 */
@Service("peritoApiService")
public class PeritoServiceImpl implements PeritoService {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(PeritoServiceImpl.class);

    @Autowired
    private PeritoRepository peritoRepository;
    
    /**
     * @see mx.gob.sat.siat.poc.ws.api.PeritoService#findPerito(Long)
     */
    @Override
    public Perito findPerito(Long idPerito) throws PeritoNotFoundException{
        LOG.debug("### >> findPerito(idPerito=[{}]) ###", idPerito);

        Perito cont = this.peritoRepository.findOne(idPerito);
        if (cont != null && cont.getIdPerito() != null) {
            LOG.debug("### << findPerito() ###");
            return cont;
        } else {
            throw new PeritoNotFoundException("Perito con id=[" +
                    idPerito.toString() + "] no se encuentra!");
        }
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.PeritoService#save(Perito)
     */
    @Override
    public List<Perito> findAll() {
        LOG.debug("### >> findAll() ###");

        List<Perito> cs = peritoRepository.findAll();

        LOG.debug("### << findAll(): [{}] ###", cs.size());
        return cs;
    }

    @Override
    public Perito save(Perito perito) {
        LOG.debug("### >> save() ###");

        LOG.debug("### >> save() ###");
        Perito cont = this.peritoRepository.saveAndFlush(perito);
        LOG.debug("### << save(): [{}] ###", cont.getIdPerito());
        return cont;
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.PeritoService#borrar(Long)
     */
    @Override
    public void borrar(Long id) throws PeritoNotFoundException {
        LOG.debug("### >> borrar() ###");
        Perito c = this.peritoRepository.findOne(id);
        if (c == null) {
            throw new PeritoNotFoundException();
        }

        this.peritoRepository.delete(id);
        LOG.debug("### <<s borrar() ###");
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.PeritoService#actualizar(Perito)
     */
    @Override
    public Perito actualizar(Perito perito) throws PeritoNotFoundException {
        LOG.info("######### PeritoApiServiceImpl.actualizar()...");
        
        Perito cont = this.peritoRepository.saveAndFlush(perito);
        if (cont != null && cont.getIdPerito() != null) {
            LOG.info("### << actualizar() ###");
            return cont;
        } else {
            throw new PeritoNotFoundException("Perito con id=[" +
                    perito.getIdPerito().toString() + "] no se encuentra!");
        }
    }

}
