/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api;

/**
 * @author Rafael Antonio Guti&eacute;rrez Turullols
 */
public class NotificacionPagoNotFoundException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -8556952761252113967L;

	public NotificacionPagoNotFoundException() {
    }

    public NotificacionPagoNotFoundException(String message) {
        super(message);
    }

    public NotificacionPagoNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotificacionPagoNotFoundException(Throwable cause) {
        super(cause);
    }
}
