/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api;

import com.creditoparati.avaluos.ws.domain.Cliente;

import java.util.List;

/**
 * Servicio de aplicacion para realizar acciones sobre el objeto de dominio Cliente.
 *
 * @author Edgar Deloera
 */
public interface ClienteService {

    /**
     * Obtiene un Cliente por su id.
     * @param idCliente El id del Cliente a obtener.
     * @return El objeto Cliente con sus datos o null.
     * @throws ClienteNotFoundException
     */
    Cliente findCliente(Long idCliente) throws ClienteNotFoundException;

    /**
     * Insertar un Cliente en la BD.
     * @param Cliente El Cliente a ser insertado.
     * @return El Cliente insertado.
     */
    Cliente save(Cliente cliente);

    List<Cliente> findAll();

    /**
     * Elimina un Cliente por su id.
     * @param id El id del Cliente a eliminar.
     * @throws ClienteNotFoundException
     */
    void borrar(Long id) throws ClienteNotFoundException;

    /**
     * Actualiza un Cliente.
     * @param Cliente El Cliente con los datos a actualizar.
     * @return El Cliente actualizado.
     * @throws ClienteNotFoundException
     */
    Cliente actualizar(Cliente cliente) throws ClienteNotFoundException;

}
