package com.creditoparati.avaluos.ws.api;

import com.creditoparati.avaluos.ws.domain.Servicio;

import java.util.List;

/**
 * Servicio de aplicacion para realizar acciones sobre el objeto de dominio Servicio.
 *
 * @author Edgar Deloera
 */
public interface ServicioService {

    /**
     * Obtiene un Servicio por su id.
     * @param idServicio El id del Servicio a obtener.
     * @return El objeto Servicio con sus datos o null.
     * @throws ServicioNotFoundException
     */
    Servicio findServicio(Long idServicio) throws ServicioNotFoundException;

    /**
     * Insertar un Servicio en la BD.
     * @param Servicio El Servicio a ser insertado.
     * @return El Servicio insertado.
     */
    Servicio save(Servicio servicio);

    List<Servicio> findAll();

    /**
     * Elimina un Servicio por su id.
     * @param id El id del Servicio a eliminar.
     * @throws ServicioNotFoundException
     */
    void borrar(Long id) throws ServicioNotFoundException;

    /**
     * Actualiza un Servicio.
     * @param Servicio El Servicio con los datos a actualizar.
     * @return El Servicio actualizado.
     * @throws ServicioNotFoundException
     */
    Servicio actualizar(Servicio servicio) throws ServicioNotFoundException;

}
