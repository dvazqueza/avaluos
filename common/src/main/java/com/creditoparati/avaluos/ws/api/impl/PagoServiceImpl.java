/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api.impl;

import java.util.List;

import com.creditoparati.avaluos.ws.api.PagoNotFoundException;
import com.creditoparati.avaluos.ws.api.PagoService;
import com.creditoparati.avaluos.ws.dao.PagoRepository;
import com.creditoparati.avaluos.ws.domain.Pago;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementacion del servicio de aplicacion para realizar acciones sobre el objeto de dominio Pago.
 * @author Edgar Deloera
 */
@Service("pagoApiService")
public class PagoServiceImpl implements PagoService {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(PagoServiceImpl.class);

    @Autowired
    private PagoRepository pagoRepository;
    
    /**
     * @see mx.gob.sat.siat.poc.ws.api.PagoService#findPago(Long)
     */
    @Override
    public Pago findPago(Long idPago) throws PagoNotFoundException{
        LOG.debug("### >> findPago(idPago=[{}]) ###", idPago);

        Pago cont = this.pagoRepository.findOne(idPago);
        if (cont != null && cont.getIdPago() != null) {
            LOG.debug("### << findPago() ###");
            return cont;
        } else {
            throw new PagoNotFoundException("Pago con id=[" +
                    idPago.toString() + "] no se encuentra!");
        }
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.PagoService#save(Pago)
     */
    @Override
    public List<Pago> findAll() {
        LOG.debug("### >> findAll() ###");

        List<Pago> cs = pagoRepository.findAll();

        LOG.debug("### << findAll(): [{}] ###", cs.size());
        return cs;
    }

    @Override
    public Pago save(Pago pago) {
        LOG.debug("### >> save() ###");

        LOG.debug("### >> save() ###");
        Pago cont = this.pagoRepository.saveAndFlush(pago);
        LOG.debug("### << save(): [{}] ###", cont.getIdPago());
        return cont;
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.PagoService#borrar(Long)
     */
    @Override
    public void borrar(Long id) throws PagoNotFoundException {
        LOG.debug("### >> borrar() ###");
        Pago c = this.pagoRepository.findOne(id);
        if (c == null) {
            throw new PagoNotFoundException();
        }

        this.pagoRepository.delete(id);
        LOG.debug("### <<s borrar() ###");
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.PagoService#actualizar(Pago)
     */
    @Override
    public Pago actualizar(Pago pago) throws PagoNotFoundException {
        LOG.info("######### PagoApiServiceImpl.actualizar()...");
        
        Pago cont = this.pagoRepository.saveAndFlush(pago);
        if (cont != null && cont.getIdPago() != null) {
            LOG.info("### << actualizar() ###");
            return cont;
        } else {
            throw new PagoNotFoundException("Pago con id=[" +
                    pago.getIdPago().toString() + "] no se encuentra!");
        }
    }

}
