/*
 *  Todos los Derechos Reservados (c) 2013 SAT.
 *  Servicio de Administracion Tributaria (SAT).
 *
 *  Este software contiene informacion propiedad exclusiva del SAT considerada
 *  Confidencial. Queda totalmente prohibido su uso o divulgacion en forma
 *  parcial o total.
 */
package com.creditoparati.avaluos.ws.api.impl;

import java.util.List;

import com.creditoparati.avaluos.ws.api.ControladorNotFoundException;
import com.creditoparati.avaluos.ws.api.ControladorService;
import com.creditoparati.avaluos.ws.dao.ControladorRepository;
import com.creditoparati.avaluos.ws.domain.Controlador;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementacion del servicio de aplicacion para realizar acciones sobre el objeto de dominio Controlador.
 * @author Edgar Deloera
 */
@Service("controladorApiService")
public class ControladorServiceImpl implements ControladorService {

    /**
     * Logging.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ControladorServiceImpl.class);

    @Autowired
    private ControladorRepository controladorRepository;
    
    /**
     * @see mx.gob.sat.siat.poc.ws.api.ControladorService#findControlador(Long)
     */
    @Override
    public Controlador findControlador(Long idControlador) throws ControladorNotFoundException{
        LOG.debug("### >> findControlador(idControlador=[{}]) ###", idControlador);

        Controlador cont = this.controladorRepository.findOne(idControlador);
        if (cont != null && cont.getIdControlador() != null) {
            LOG.debug("### << findControlador() ###");
            return cont;
        } else {
            throw new ControladorNotFoundException("Controlador con id=[" +
                    idControlador.toString() + "] no se encuentra!");
        }
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.ControladorService#save(Controlador)
     */
    @Override
    public List<Controlador> findAll() {
        LOG.debug("### >> findAll() ###");

        List<Controlador> cs = controladorRepository.findAll();

        LOG.debug("### << findAll(): [{}] ###", cs.size());
        return cs;
    }

    @Override
    public Controlador save(Controlador controlador) {
        LOG.debug("### >> save() ###");

        LOG.debug("### >> save() ###");
        Controlador cont = this.controladorRepository.saveAndFlush(controlador);
        LOG.debug("### << save(): [{}] ###", cont.getIdControlador());
        return cont;
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.ControladorService#borrar(Long)
     */
    @Override
    public void borrar(Long id) throws ControladorNotFoundException {
        LOG.debug("### >> borrar() ###");
        Controlador c = this.controladorRepository.findOne(id);
        if (c == null) {
            throw new ControladorNotFoundException();
        }

        this.controladorRepository.delete(id);
        LOG.debug("### <<s borrar() ###");
    }

    /**
     * @see mx.gob.sat.siat.poc.ws.api.ControladorService#actualizar(Controlador)
     */
    @Override
    public Controlador actualizar(Controlador controlador) throws ControladorNotFoundException {
        LOG.info("######### ControladorApiServiceImpl.actualizar()...");
        
        Controlador cont = this.controladorRepository.saveAndFlush(controlador);
        if (cont != null && cont.getIdControlador() != null) {
            LOG.info("### << actualizar() ###");
            return cont;
        } else {
            throw new ControladorNotFoundException("Controlador con id=[" +
                    controlador.getIdControlador().toString() + "] no se encuentra!");
        }
    }

}
